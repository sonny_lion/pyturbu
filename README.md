=====
Pyturbu
=====

.. contents::

What is Pyturbu?
--------------

Pyturbu is a Python toolbox for studying turbulence


Installation
------------

For installation instructions, see ``INSTALL.txt``.


Documentation
-------------

Pyturbu documentation is available in the doc folder: ``doc/README.txt``.


Latest source code
------------------

The latest development version of pyturbu's sources are always available at:

    https://bitbucket.org/sonny_lion/pyturbu

They can be downloaded as a zip file or using the Git client.


Bug reports
-----------

To report bugs, please send a mail at:

    sonny.lion@obspm.fr


License information
-------------------

See the file ``LICENSE.txt`` for information on the history of this
software, terms & conditions for usage, and a DISCLAIMER OF ALL
WARRANTIES.
