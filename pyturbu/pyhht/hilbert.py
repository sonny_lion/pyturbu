#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: slion
# @Date:   2015-04-13 09:52:12
# @Last Modified by:   slion
# @Last Modified time: 2015-09-21 15:39:56

import numpy
import matplotlib.pyplot as plt
import scipy.signal
from scipy import interpolate
import emd


def calc_inst_info(modes, samplerate):
    """
    Calculate the instantaneous frequency, amplitude, and phase of
    each mode.

    samplerate in Hz
    """

    amplitude = numpy.zeros(modes.shape, numpy.float32)
    phase = numpy.zeros(modes.shape, numpy.float32)
    pulsation = numpy.zeros(modes.shape, numpy.float32)

    for m in range(len(modes)):
        z = scipy.signal.hilbert(modes[m])
        amplitude[m, :] = numpy.abs(z)
        phase[m, :] = numpy.angle(z)
        re_z = numpy.real(z)
        im_z = numpy.imag(z)
        dxt = numpy.gradient(re_z)
        dyt = numpy.gradient(im_z)

        pulsation[m, :] = samplerate*(re_z*dyt - im_z*dxt)/((re_z**2) + (im_z**2))

    return pulsation/(numpy.pi*2), amplitude, phase


def spectrogram(t, instanteneous_frequency, amplitude):
    plt.figure(figsize=[4, 3])
    time = numpy.zeros(instanteneous_frequency.shape, numpy.float32)
    for i_mode, mode in enumerate(instanteneous_frequency):
        time[i_mode, :] = t
    ax = plt.gca()
    x = numpy.ravel(time)
    y = numpy.ravel(instanteneous_frequency)
    z = numpy.ravel(amplitude*amplitude)
    min_freq = numpy.log10(numpy.min(y[y > 0]))
    max_freq = numpy.log10(numpy.max(y[y > 0]))
    # grid_z0 = interpolate.griddata(numpy.array([x, y]).T, z, (grid_x, grid_y), method='cubic')
    H, xedges, yedges = numpy.histogram2d(x, y, bins=[t[::10], numpy.logspace(min_freq, max_freq, 200)], weights=z)
    # grid_x, grid_y = numpy.meshgrid(xedges, yedges, sparse=False, indexing='ij')
    colormesh = ax.pcolormesh(xedges, yedges, H.T, cmap='Greens')
    cbar = plt.colorbar(colormesh)

    # ### plt.scatter(time, instanteneous_frequency, s=3, c=numpy.log10(amplitude*amplitude), alpha=0.5)
    # plt.imshow(numpy.log10(numpy.ma.masked_where(H == 0, H).T), interpolation='none', origin='lower', extent=extent)
    # cset = plt.contour(numpy.log10(H), origin="lower", extent=extent)
    colormesh.set_rasterized(True)  # Bitmap pour pouvoir sortir les fichiers pdf plus vite
    ax.set_xlim([xedges.min(), xedges.max()])
    ax.set_yscale('log')
    ax.set_ylim([yedges.min(), yedges.max()])


def main():
    # load data

    import pyturbu.read.probe_data as probe_data
    import datetime

    dataset = 4

    if dataset == 1:
        start_Epoch = datetime.datetime(1995, 1, 30, 13, 0)
        end_Epoch = datetime.datetime(1995, 1, 30, 13, 15)

        probe_d = probe_data.Probe_data("WIND", start_Epoch, end_Epoch)
        Bz, Bx, By = probe_d.get_lf_magnetic_data()

        data = Bz

        t = numpy.arange(len(Bz))*0.184
    elif dataset == 2:

        ndat = 3000
        t = numpy.linspace(0, 9, ndat)

        data = numpy.sin(t*numpy.pi) + numpy.sin(2*t*numpy.pi) + numpy.sin(6*t*numpy.pi) + 0.5*t

    elif dataset == 3:

        ndat = 3000
        t = numpy.linspace(0, 9, ndat)

        data = numpy.sin(t*numpy.pi) + numpy.sin(2*t*numpy.pi) + numpy.sin(6*t*numpy.pi) + 0.5*t
        data[:1000] += numpy.sin(t[:1000]*3*numpy.pi)
        data[2000:] += numpy.sin(t[2000:]*3*numpy.pi)


    elif dataset == 4:

        ndat = 3000
        t = numpy.linspace(0, 9, ndat)

        data = numpy.sin(t*0)
        data[:1000] += numpy.sin(t[:1000]*3*numpy.pi)
        data[2000:] += numpy.sin(t[2000:]*3*numpy.pi)

    plt.figure()

    plt.plot(t, data)

    plt.figure()


    residue, modes = emd.eemd(data, check=False)

    samplerate = 1./(t[1]-t[0])
    period, amplitude, phase = calc_inst_info(modes, samplerate)
    time = numpy.zeros(period.shape, numpy.float32)
    for i_mode, mode in enumerate(period):
        plt.subplot(period.shape[0], 1, i_mode)
        plt.plot(t, modes[i_mode])
        print(time.shape)
        print(t.shape)
        time[i_mode, :] = t
    plt.figure()
    print(time.shape)
    print(period.shape)
    print(amplitude.shape)
    spectrogram(t, time, period, amplitude)
    #plt.yscale("log")

    plt.show()

if __name__ == '__main__':
    main()
