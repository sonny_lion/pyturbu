#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: slion
# @Date:   2015-04-15 10:59:02
# @Last Modified by:   slion
# @Last Modified time: 2015-09-24 13:42:58

import numpy
import matplotlib.pyplot as plt
import scipy.interpolate


def rep(value, n):
    return [value for x in xrange(0, n)]


def extrema(y):
    ndata = len(y)
    minindex = list()
    maxindex = list()
    nextreme = 0
    cross = list()
    ncross = 0

    z1 = numpy.sign(numpy.diff(y))

    index1 = numpy.arange(len(z1))[z1 != 0]  # give none value if z1 empty
    z1 = z1[z1 != 0]

    if not (numpy.all(z1 == 1) or
            numpy.all(z1 == -1)):

        index1 = index1[numpy.concatenate((z1[:-1] != z1[1:], [False]))] + 1
        z1 = z1[numpy.concatenate((z1[:-1] != z1[1:], [False]))]
        nextreme = len(index1)

        if(nextreme >= 2):
            for i in xrange(0, nextreme-1):
                tmpindex = numpy.arange(index1[i], index1[i + 1])
                if(z1[i] > 0):
                    tmpindex = tmpindex[y[index1[i]] == y[tmpindex]]
                    maxindex.append(numpy.array([numpy.min(tmpindex), numpy.max(tmpindex)]))
                else:
                    tmpindex = tmpindex[y[index1[i]] == y[tmpindex]]
                    minindex.append(numpy.array([numpy.min(tmpindex), numpy.max(tmpindex)]))

        tmpindex = numpy.arange(index1[nextreme-1], ndata - 1)
        if(z1[nextreme-1] > 0):
            tmpindex = tmpindex[y[index1[nextreme-1]] == y[tmpindex]]
            maxindex.append(numpy.array([numpy.min(tmpindex), numpy.max(tmpindex)]))
        else:
            tmpindex = tmpindex[y[index1[nextreme-1]] == y[tmpindex]]
            minindex.append(numpy.array([numpy.min(tmpindex), numpy.max(tmpindex)]))

        # Finding the index of zero crossing

        if not (numpy.all(numpy.sign(y) >= 0) or numpy.all(numpy.sign(y) <= 0) or numpy.all(numpy.sign(y) == 0)):
            index1 = numpy.concatenate(([0], index1))
            for i in xrange(0, nextreme):
                if (y[index1[i]] == 0):
                    tmp = numpy.arange(index1[i], index1[i + 1])[y[index1[i]:index1[i + 1]] == 0]
                    cross.append(numpy.array([numpy.min(tmp), numpy.max(tmp)]))
                elif (y[index1[i]] * y[index1[i + 1]] < 0):
                    tmp = numpy.min(numpy.arange(index1[i], index1[i + 1] + 1)[y[index1[i]] * y[index1[i]:(index1[i + 1] + 1)] <= 0])
                    if (y[tmp] == 0):
                        tmp = numpy.arange(tmp, index1[i + 1] + 1)[y[tmp:(index1[i + 1] + 1)] == 0]
                        cross.append(numpy.array([numpy.min(tmp), numpy.max(tmp)]))
                    else:
                        cross.append(numpy.array([tmp - 1, tmp]))

            # if (y[ndata] == 0) {
            #     tmp = c(index1[nextreme+1]:ndata)[y[index1[nextreme+1]:ndata] == 0]
            #     cross = rbind(cross, c(min(tmp), max(tmp)))
            # } else
            if (numpy.any(y[index1[nextreme]] * y[index1[nextreme]:ndata] <= 0)):
                tmp = numpy.min(numpy.arange(index1[nextreme], ndata)[y[index1[nextreme]] * y[index1[nextreme]:ndata] <= 0])
                if (y[tmp] == 0):
                    tmp = numpy.arange(tmp, ndata)[y[tmp:ndata] == 0]
                    cross.append(numpy.array([numpy.min(tmp), numpy.max(tmp)]))
                else:
                    cross.append(numpy.array([tmp - 1, tmp]))
            ncross = len(cross)

    return dict(minindex=numpy.array(minindex), maxindex=numpy.array(maxindex), nextreme=nextreme, cross=numpy.array(cross), ncross=ncross)


def extractimf(residue, tolerance=None, max_sift=200, stoprule="type1", boundary="wave", sm="none", spar=None, check=False):
    if tolerance is None:
        tolerance = numpy.std(residue)*0.1**2

    if (boundary == "none"):
        minextrema = 4
    else:
        minextrema = 2

    if ((sm == "spline" or sm == "kernel" or sm == "locfit") and (spar is None or spar == 0)):
        print("Provide the smoothing parameter.\n")  # || sm == "quantile"
        exit()

    ndata = len(residue)
    # ndatam1 = ndata - 1

    tt = numpy.arange(ndata)

    emin = list()
    emax = list()
    em = list()
    h = list()
    imf = list()
    debug = False

    # n2data = 2*ndata
    # tt2 = numpy.arange(n2data)
    # n3data = 3*ndata
    # n3datam1 = n3data-1
    # tt3 = numpy.arange(n3data)

    data_input = residue
    # rangext = numpy.array([numpy.min(residue), numpy.max(residue)])
    j = 0
    s_count = 0
    prev_excross = 0

    while(True):
        tmp = extrema(data_input)
        if check:
            plt.figure()
            ax1 = plt.subplot(2, 1, 1)
            plt.plot(data_input)
            time = numpy.arange(len(data_input))
            plt.plot(time[tmp["maxindex"]], data_input[tmp["maxindex"]], "o", color="red")
            plt.plot(time[tmp["minindex"]], data_input[tmp["minindex"]], "o", color="blue")
        if (tmp["nextreme"] <= minextrema):
            break

        if(j == 1 or boundary == "wave"):
            minindex = numpy.unique(tmp["minindex"])
            minn = len(minindex)
            maxindex = numpy.unique(tmp["maxindex"])
            maxn = len(maxindex)

            extminindex = minindex
            extmaxindex = maxindex
            tmpwavefreq1 = numpy.diff(numpy.sort(numpy.array([tt[0], tt[minindex[0]], tt[maxindex[0]]])))
            tmpwavefreq2 = numpy.diff(numpy.sort(numpy.array([tt[minindex[minn-1]], tt[maxindex[maxn-1]], tt[ndata-1]])))

            if(data_input[0] <= data_input[minindex[0]] and data_input[0] <= data_input[maxindex[0]]):
                extminindex = numpy.concatenate(([0], extminindex))
                wavefreq1 = 2 * tmpwavefreq1[0]
            elif(data_input[0] >= data_input[minindex[0]] and data_input[0] >= data_input[maxindex[0]]):
                extmaxindex = numpy.concatenate(([0], extmaxindex))
                wavefreq1 = 2 * tmpwavefreq1[0]
            elif(data_input[0] >= (data_input[minindex[0]] + data_input[maxindex[0]])*0.5):
                wavefreq1 = tmpwavefreq1[1] + max(tmpwavefreq1[1], 2*tmpwavefreq1[0])
            else:
                wavefreq1 = tmpwavefreq1[1] + max(tmpwavefreq1[1], numpy.round(1.5*tmpwavefreq1[0]))

            if(data_input[ndata-1] <= data_input[minindex[minn-1]] and data_input[ndata-1] <= data_input[maxindex[maxn-1]]):
                extminindex = numpy.concatenate((extminindex, [ndata-1]))
                wavefreq2 = 2 * tmpwavefreq2[1]
            elif(data_input[ndata-1] >= data_input[minindex[minn-1]] and data_input[ndata-1] >= data_input[maxindex[maxn-1]]):
                extmaxindex = numpy.concatenate((extmaxindex, [ndata-1]))
                wavefreq2 = 2 * tmpwavefreq2[1]
            elif(data_input[ndata-1] >= (data_input[minindex[minn-1]] + data_input[maxindex[maxn-1]])*0.5):
                wavefreq2 = tmpwavefreq2[0] + max(tmpwavefreq2[0], 2*tmpwavefreq2[1])
            else:
                wavefreq2 = tmpwavefreq2[0] + max(tmpwavefreq2[0], numpy.round(1.5*tmpwavefreq2[1]))

            extminn = len(extminindex)
            extmaxn = len(extmaxindex)

            extttminindex = numpy.concatenate((tt[extminindex[0]] - numpy.arange(1, 5)[::-1] * wavefreq1,
                                              tt[extminindex],
                                              tt[extminindex[extminn-1]] + numpy.arange(1, 5) * wavefreq2))
            extttmaxindex = numpy.concatenate((tt[extmaxindex[0]] - numpy.arange(1, 5)[::-1] * wavefreq1,
                                              tt[extmaxindex],
                                              tt[extmaxindex[extmaxn-1]] + numpy.arange(1, 5) * wavefreq2))

            if(sm == "none"):
                f = scipy.interpolate.splrep(extttminindex,
                                             numpy.concatenate((rep(data_input[extminindex[0]], 4),
                                                                data_input[extminindex],
                                                                rep(data_input[extminindex[extminn-1]], 4)
                                                                )), s=.0)
                if debug:
                    print(numpy.concatenate((rep(data_input[extminindex[0]], 4),
                                             data_input[extminindex],
                                             rep(data_input[extminindex[extminn-1]], 4)
                                             )))
                    print(extttminindex)
                    print(scipy.interpolate.splev(tt, f))
                emin.append(scipy.interpolate.splev(tt, f))

                f = scipy.interpolate.splrep(extttmaxindex,
                                             numpy.concatenate((rep(data_input[extmaxindex[0]], 4),
                                                                data_input[extmaxindex],
                                                                rep(data_input[extmaxindex[extmaxn-1]], 4)
                                                                )), s=.0)
                emax.append(scipy.interpolate.splev(tt, f))

            if debug:
                print(emin)
            em.append((emin[j] + emax[j])*0.5)
        h.append(data_input - em[j])

        if (stoprule == "type1" and (numpy.all(numpy.abs(em[j]) < tolerance) or j >= max_sift)):
            imf = h[j]
            residue = residue - imf
            break
        elif (stoprule == "type5" and j >= 2):
                if (numpy.abs(tmp["nextreme"] - tmp["ncross"]) <= 1 and tmp["nextreme"]+tmp["ncross"] == prev_excross):
                    s_count += 1
                    if (s_count >= tolerance or j >= max_sift):
                        imf = h[j]
                        residue = residue - imf
                        break
                prev_excross = tmp["nextreme"]+tmp["ncross"]

        if check:
            plt.subplot(2, 1, 2, sharex=ax1, sharey=ax1)
            plt.plot(emin[j], color="blue")
            plt.plot(emax[j], color="red")
            plt.plot(time[tmp["maxindex"]], data_input[tmp["maxindex"]], "o", color="red")
            plt.plot(time[tmp["minindex"]], data_input[tmp["minindex"]], "o", color="blue")
            plt.plot(em[j], color="black")
            plt.plot(h[j]+em[j], color="green")
            plt.show()
        data_input = h[j]
        j += 1
    if(check):
        return dict(emin=emin, emax=emax, em=em, h=h, imf=imf, residue=residue, niter=j)
    else:
        return dict(imf=imf, residue=residue, niter=j)


def emd(data, tol=None, check=False, stoprule="type5", boundary="wave", max_sift=100, max_imf=50):
    residue = data
    modes = list()
    nimf = 0
    while(True):
        if nimf == max_imf:
            break
        dict_imf = extractimf(residue, tolerance=tol, boundary=boundary, check=check, stoprule=stoprule, max_sift=100)
        if dict_imf["imf"] == []:
            break
        nimf += 1
        modes.append(dict_imf["imf"])
        residue = dict_imf["residue"]
    emd_result = {}
    emd_result["imf"] = numpy.array(modes)
    emd_result["residue"] = numpy.array(residue)
    emd_result["nimf"] = nimf
    return emd_result


def eemd(data, check=True, nb_trial=10, noise_lvl=0.001, stoprule="type1"):
    """The effects of the decomposition using the EEMD are that the added white noise series
    cancel each other, and the mean IMFs stays within the natural dyadic filter windows,
    significantly reducing the chance of mode mixing and preserving the dyadic property."""

    noise = noise_lvl*numpy.random.randn(nb_trial, len(data))

    residue, imfs = emd(data + noise[0], check=check)

    for trial in xrange(1, nb_trial):
        residue_tmp, imfs_tmp = emd(data + noise[trial], check=check, stoprule=stoprule)
        residue += residue_tmp
        imfs += imfs_tmp

    return residue/nb_trial, imfs/nb_trial

if __name__ == '__main__':
    ndat = 3000
    t = numpy.linspace(0, 9, ndat)
    data = numpy.sin(t*numpy.pi) + numpy.sin(2*t*numpy.pi) + numpy.sin(6*t*numpy.pi) + 0.5*t
    residue, modes = emd(data, check=False, stoprule="type5")
    for idx_mode, mode in enumerate(modes):
        plt.subplot(len(modes)+1, 1, idx_mode+1)
        plt.plot(t, mode)
    plt.subplot(len(modes)+1, 1, len(modes)+1)
    plt.plot(t, residue)
    plt.show()

    exit()

    plt.plot(t, data)

    extr = extrema(data)
    print(extr["ncross"])
    print(extr["cross"])

    plt.plot(t[extr["minindex"][:,0]], data[extr["minindex"][:,0]],"o",color="green")
    plt.plot(t[extr["maxindex"][:,0]], data[extr["maxindex"][:,0]],"o",color="red")
    plt.plot(t[extr["cross"][:,0]], data[extr["cross"][:,0]],"o",color="black")
    plt.show()

    up_spline = scipy.interpolate.splrep(t[extr["maxindex"][:,0]], data[extr["maxindex"][:,0]], s=.0)
    low_spline = scipy.interpolate.splrep(t[extr["minindex"][:,0]], data[extr["minindex"][:,0]], s=.0)
    y_up = scipy.interpolate.splev(t, up_spline)
    y_low = scipy.interpolate.splev(t, low_spline)
    imf1 = 0.5*(y_up+y_low)

    plt.plot(t, data)
    plt.plot(t, y_up)
    plt.plot(t, y_low)

    plt.figure()
    plt.plot(t,imf1)
    plt.show()
