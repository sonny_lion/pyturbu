#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: slion
# @Date:   2015-04-27 15:51:31
# @Last Modified by:   slion
# @Last Modified time: 2015-05-22 16:55:30

import numpy
import logging
import matplotlib.pyplot as plt
import multiprocessing
import emd

# Description
"""
This function implements the complete ensemble empirical mode decomposition (CEEMD) algo-
rithm.
"""

# Details
"""
This function performs the complete ensemble empirical mode decomposition, a noise assisted
empirical mode decomposition algorithm. The CEEMD works by adding a certain amplitude of
white noise to a time series, decomposing it via EMD, and saving the result. In contrast to the
Ensemble Empirical Mode Decomposition (EEMD) method, the CEEMD also ensures that the IMF
set is quasi-complete and orthogonal. The CEEMD can ameliorate mode mixing and intermittency
problems. Keep in mind that the CEEMD is a computationally expensive algorithm and may take
significant time to run.
"""

# Copyright
"""
This is derived from the code written by Daniel Bowman <daniel.bowman@unc.edu> in R
"""

# References

"""
Torres, M. E., Colominas, M. A., Schlotthauer, G., Flandrin, P. (2011). A complete ensemble empir-
ical mode decomposition with adaptive noise. 2011 IEEE International Conference on Acoustics,
Speech, and Signal Processing, pp.4144-4147, doi: 10.1109/ICASSP.2011.5947265.
"""


def sub_ceemd(k, r, tt, noise_imfs, n_i, **kwargs):
    noise_imf = numpy.zeros(len(tt))
    if (noise_imfs[k].shape[0] < n_i):
        logging.warning("Attempted to extract more IMFs from the signal than are present in the noise series for trial "+str(k)+".")
        noise_imf = numpy.zeros(len(tt))
    else:
        noise_imf = noise_imfs[k][n_i-1]

    if (emd.extrema(r + noise_imf)["nextreme"] > 2):
        return Sig2IMF(r[:] + noise_imf[:], tt, kwargs)["imf"][0]
    else:
        return noise_imf


def ceemd(sig, tt, noise_amp, trials, noise_type="gaussian",
          noise_array=None, max_imf=20, tolerance=5, stoprule="type5",
          use_R=False, boundary="wave", processes=None):

    parallel_computing = processes is not None

    if parallel_computing:
        pool = multiprocessing.Pool(processes=processes)

    # Make noise matrix ...

    if noise_type not in ["uniform", "gaussian", "custom"]:
        error_msg = ("Did not recognise noise_type option " +
                     noise_type +
                     " Please choose either ''uniform'' or ''gaussian''")
        try:
            raise ValueError(error_msg)
        except ValueError:
            logging.error(error_msg)
            raise

    if (noise_type == "custom"):
        if noise_array:
            if noise_array.shape != (trials, len(tt)):
                error_msg = """You requested a custom noise array but either the number of rows did not equal
                               the number of CEEMD trials or the number of columns did not equal the signal length,
                               or both."""
                try:
                    raise ValueError(error_msg)
                except ValueError:
                    logging.error(error_msg)
                    raise
            noise = noise_array

        else:
            error_msg = """If noise.type = "custom", then you must set noise.array equal
                           to an array with the same number of rows as CEEMD trials and
                           the same number of columns as signal samples."""
            try:
                raise ValueError(error_msg)
            except ValueError:
                logging.error(error_msg)
                raise

    elif (noise_type == "uniform"):
        noise = noise_amp*numpy.random.rand(trials, len(tt))
        noise -= numpy.mean(noise)

    elif (noise_type == "gaussian"):
        noise = noise_amp*numpy.random.randn(trials, len(tt))

    # ... and extract IMF 1

    logging.info("Extracting IMF 1 from each noise/signal realization...")

    imfs = [numpy.zeros(len(tt))]

    kw = {'tol': tolerance, 'stop_rule': stoprule, 'boundary': boundary, 'max_imf': 1, "use_R": use_R}

    if parallel_computing:
        results = [pool.apply_async(Sig2IMF, args=(sig[:] + noise[k, :], tt), kwds=kw) for k in xrange(0, trials)]
        output = [p.get()["imf"][0] for p in results]
        imfs[0] = numpy.mean(output, axis=0)
    else:
        for k in xrange(0, trials):
            imfs[0] += Sig2IMF(sig[:] + noise[k, :], tt, **kw)["imf"][0]
            logging.info("Trial "+str(k)+" complete.")

        imfs[0] /= trials

    logging.info("IMF 1 extracted.")

    # Now decompose the noise into IMFs

    logging.info("Decomposing noise series...")

    kw = {'tol': tolerance, 'stop_rule': stoprule, 'boundary': boundary, 'max_imf': max_imf, "use_R": use_R}

    if parallel_computing:
        results = [pool.apply_async(Sig2IMF, args=(noise[k, :], tt), kwds=kw) for k in xrange(0, trials)]
        noise_imfs = [p.get()["imf"] for p in results]
        logging.info("Noise trial all complete.")
    else:
        noise_imfs = []
        for k in xrange(0, trials):
            noise_imfs.append(Sig2IMF(noise[k, :], tt, **kw)["imf"])
            logging.info("Noise trial "+str(k)+" complete.")

    # Continue extracting IMFs until either the maximum limit of IMFs is reached or the signal no longer has oscillatory elements
    r = sig - imfs[0]
    n_i = 1

    kw = {'tol': tolerance, 'stop_rule': stoprule, 'boundary': boundary, 'max_imf': 1, "use_R": use_R}

    while (n_i < max_imf and emd.extrema(r)["nextreme"] > 2):
        imf_avg = numpy.zeros(len(tt))
        if parallel_computing:
            results = [pool.apply_async(sub_ceemd, args=(k, r, tt, noise_imfs, n_i,), kwds=kw) for k in xrange(0, trials)]
            output = [p.get() for p in results]
            imf_avg = numpy.mean(output, axis=0)
            logging.info("IMF "+str(n_i + 1)+" for all TRIALs")
        else:
            for k in xrange(0, trials):
                imf_avg += sub_ceemd(k, r, tt, noise_imfs, n_i, **kw)
                logging.info("IMF "+str(n_i + 1)+" TRIAL "+str(k))
            imf_avg /= trials  # Average the IMF trials together

        imfs.append(imf_avg)

        r -= imf_avg
        n_i += 1

    return r, numpy.array(imfs)[:n_i, :len(tt)]


def R_ceemd(sig, tt, noise_amp, trials):
    import rpy2.robjects.packages as rpackages
    import rpy2.robjects.numpy2ri
    rpy2.robjects.numpy2ri.activate()
    R_hht = rpackages.importr('hht')
    result = R_hht.CEEMD(sig, tt, noise_amp, trials)
    residue = numpy.array(result[1])
    # nb_imf = int(result[-1][0])
    imfs = numpy.array(result[-2])
    return residue, imfs.T


def Sig2IMF(sig, tt, spectral_method="arctan",
            diff_lag=1, stop_rule="type5", tol=5,
            boundary="wave", sm="none", smlevels=[1],
            spar=None, max_sift=200, max_imf=100, interm=None, use_R=True):

    if use_R:
        import rpy2.robjects.packages as rpackages
        import rpy2.robjects.numpy2ri
        from rpy2 import robjects as robj
        rpy2.robjects.numpy2ri.activate()
        R_emd = rpackages.importr('EMD')
        if spar is None:
            spar_bis = robj.r("NULL")
        else:
            spar_bis = spar
        tmp_emd_result = R_emd.emd(sig, tt, max_sift=max_sift, stoprule=stop_rule, tol=tol,
                                   boundary=boundary, sm=sm, spar=spar_bis,
                                   check=False, plot_imf=False, max_imf=max_imf)
        emd_result = {}

        emd_result["imf"] = numpy.array(tmp_emd_result[0]).T
        emd_result["residue"] = numpy.array(tmp_emd_result[1])
        emd_result["nimf"] = tmp_emd_result[2][0]
        emd_result["original"] = sig
        emd_result["tt"] = tt
        emd_result["max_sift"] = max_sift
        emd_result["tol"] = tol
        emd_result["stop_rule"] = stop_rule
        emd_result["boundary"] = boundary
        emd_result["sm"] = sm
        emd_result["smlevels"] = smlevels
        emd_result["spar"] = spar
        emd_result["max_imf"] = max_imf
        emd_result["interm"] = interm
        return emd_result
    else:
        emd_result = emd.emd(sig, max_sift=max_sift, stoprule=stop_rule, tol=tol,
                             boundary=boundary,
                             check=False, max_imf=max_imf)
        emd_result["original"] = sig
        emd_result["tt"] = tt
        emd_result["max_sift"] = max_sift
        emd_result["tol"] = tol
        emd_result["stop_rule"] = stop_rule
        emd_result["boundary"] = boundary
        emd_result["sm"] = sm
        emd_result["smlevels"] = smlevels
        emd_result["spar"] = spar
        emd_result["max_imf"] = max_imf
        emd_result["interm"] = interm
        return emd_result


if __name__ == '__main__':
    import argparse
    import timeit

    start_time = timeit.default_timer()

    parser = argparse.ArgumentParser()
    parser.add_argument('-R', help='Use R and python to compute the EMD', action="store_true")
    parser.add_argument('-Ronly', help='Use R only to compute the EMD', action="store_true")
    parser.add_argument('-p', '--processor', help='Use parallel computing with P processor', type=int)
    args = parser.parse_args()
    print(args)
    # data(PortFosterEvent)
    ndat = 10000
    # numpy.random.seed(15)
    tt = numpy.linspace(0, 9, ndat)
    sig = numpy.sin(tt*numpy.pi) + numpy.sin(2*tt*numpy.pi) + numpy.sin(6*tt*numpy.pi) + 0.5*tt
    noise_amp = 0.1*numpy.std(sig)
    sig += noise_amp*numpy.random.rand(len(tt))
    trials = 5
    if args.Ronly:
        plt.figure()
        nb_imf, imfs = R_ceemd(sig, tt, noise_amp, trials)
        print("nb_imf: ", nb_imf)
        plt.subplot(nb_imf+1, 1, 1)
        plt.plot(tt, sig)
        for i_imf in xrange(0, nb_imf):
            plt.subplot(nb_imf+1, 1, i_imf+2)
            plt.plot(tt, imfs[:, i_imf])
        plt.show()
    else:

        plt.figure()

        logging.basicConfig(level=logging.DEBUG)
        ceemd_result = ceemd(sig, tt, noise_amp, trials, tolerance=5, max_imf=12, processes=args.processor, use_R=args.R)
        for idx_mode, mode in enumerate(ceemd_result):
            plt.subplot(len(ceemd_result)+1, 1, idx_mode+1)
            plt.plot(tt, mode)

        stop_time = timeit.default_timer()

        print(stop_time - start_time)

        plt.show()
