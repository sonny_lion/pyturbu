#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: slion
# @Date:   2016-05-30 11:45:21
# @Last Modified by:   slion
# @Last Modified time: 2016-05-30 11:49:46

import numpy


def compute_spectrum(details_coeff):
    return [numpy.mean(numpy.power(numpy.abs(coeff), 2)) for coeff in details_coeff]


def compute_frequency(data_sampling_freq, dwt_max_level):
    return data_sampling_freq/(1.+2**numpy.arange(dwt_max_level))[::-1]
