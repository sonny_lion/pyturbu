#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: Sonny
# @Date:   2015-12-15 14:47:48
# @Last Modified by:   Sonny
# @Last Modified time: 2015-12-15 15:37:05

import pywt
import numpy
import matplotlib.pyplot as plt

def dwt(data, wlt=pywt.Wavelet('coif2')):
	coeffs = pywt.wavedec(data, wlt, "per", level=None)
	approximation_coeffs = coeffs[0]  # cA_n
	detail_coeffs = coeffs[1:]  # cD_n_to_1
	return approximation_coeffs, detail_coeffs

def dwt_frequency(data_sampling_freq, dwt_max_level):
	return data_sampling_freq/(1.+2**numpy.arange(dwt_max_level))[::-1]

def save(filename, approximation_coeffs, detail_coeffs, data_sampling_freq):
	numpy.savez_compressed(filename, a=approximation_coeffs, d=numpy.concatenate(detail_coeffs), f=data_sampling_freq)

def load(filename):
	with numpy.load(filename) as data:
	    approximation_coeffs = data['a']
	    details_concat = data['d']
	    data_sampling_freq = data['f']
	n_approx = len(approximation_coeffs)
	detail_coeffs = [details_concat[:n_approx]]
	n_min = n_approx
	n_max = n_approx+n_approx*2
	while n_max <= len(details_concat):
		detail_coeffs.append(details_concat[n_min:n_max])
		n_min = n_max
		n_max = 2*n_max + n_approx

	freq = dwt_frequency(data_sampling_freq, len(detail_coeffs))
	return approximation_coeffs, detail_coeffs, freq

if __name__ == '__main__':
	data = numpy.arange(2**12)
	approximation_coeffs, detail_coeffs = dwt(data)
	filename = "toto.npz"
	data_sampling_freq = 8.
	save(filename, approximation_coeffs, detail_coeffs, data_sampling_freq)
	approximation_coeffs2, detail_coeffs2, freq = load(filename)

	print("max diff a:", max(numpy.abs(approximation_coeffs2-approximation_coeffs)))
	for x in xrange(0, 8):
		print("max diff d:", max(numpy.abs(detail_coeffs2[x]-detail_coeffs[x])))

	print("freq:", freq)



