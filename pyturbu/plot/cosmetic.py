#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: slion
# @Date:   2015-01-28 19:30:10
# @Last Modified by:   slion
# @Last Modified time: 2016-02-09 18:08:02

import matplotlib
import os
import matplotlib.pyplot as plt

from matplotlib.collections import Collection
from matplotlib.artist import allow_rasterization

import pyturbu.plot


def hide_spines(figures="all", axis_position=["right", "top"], axes=None):
    """Hides the specified axis spines from view for all specified
    figures and their respective axes."""

    mapping_table = {"top": "bottom",
                     "bottom": "top",
                     "right": "left",
                     "left": "right"}

    if axes == None:
        if figures == "all":
            figures = [x for x in matplotlib._pylab_helpers.Gcf.get_all_fig_managers()]
        for figure in figures:
            # Get all Axis instances related to the figure.
            for ax in figure.canvas.figure.get_axes():
                for position in axis_position:
                    # Disable spines.
                    ax.spines[position].set_color('none')
                    # Disable ticks.
                    if (position == "left" or position == "right"):
                        if mapping_table[position] in axis_position:
                            ax.yaxis.set_ticks_position("none")
                        else:
                            ax.yaxis.set_ticks_position(mapping_table[position])
                    else:
                        if mapping_table[position] in axis_position:
                            ax.xaxis.set_ticks_position("none")
                        else:
                            ax.xaxis.set_ticks_position(mapping_table[position])
    else:
        for ax in axes:
            for position in axis_position:
                # Disable spines.
                ax.spines[position].set_color('none')
                # Disable ticks.
                if (position == "left" or position == "right"):
                    if mapping_table[position] in axis_position:
                        ax.yaxis.set_ticks_position("none")
                    else:
                        ax.yaxis.set_ticks_position(mapping_table[position])
                else:
                    if mapping_table[position] in axis_position:
                        ax.xaxis.set_ticks_position("none")
                    else:
                        ax.xaxis.set_ticks_position(mapping_table[position])


def bmh_color_table():
    return ["#348ABD", "#A60628", "#7A68A6", "#467821", "#D55E00", "#CC79A7", "#56B4E9", "#009E73", "#F0E442", "#0072B2"]


def color_table():
    return ["#000000", "#e41a1c", "#377eb8", "#4daf4a", "#984ea3", "#ff7f00", "#DECF3F", "#a65628", "#f781bf", "#999999"]


def colorblind_table():
    return ["#000000",
            "#E69F00",
            "#56B4E9",
            "#009E73",
            "#F0E442",
            "#0072B2",
            "#D55E00",
            "#CC7CA7"]


def insert_rasterized_contour_plot(cs):
    for c in cs.collections:
        c.set_rasterized(True)
        # c.set_edgecolor('none')


def mplstyle(style_list):
    if not isinstance(style_list, str):
        matplotlib.style.use(['%s/stylelib/%s.mplstyle' % (os.path.dirname(pyturbu.plot.__file__), style) for style in style_list])
    else:
        matplotlib.style.use('%s/stylelib/%s.mplstyle' % (os.path.dirname(pyturbu.plot.__file__), style_list))
