#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: slion
# @Date:   2015-02-18 11:57:12
# @Last Modified by:   slion
# @Last Modified time: 2015-03-24 14:45:11

import matplotlib.pyplot as plt
import numpy


def hodogram(x, y, ax=None, **kwargs):
    if ax is None:
        ax = plt.axes()
    x = numpy.array(x)
    y = numpy.array(y)
    ax.quiver(x[:-1], y[:-1], x[1:] - x[:-1], y[1:] - y[:-1], scale_units='xy', angles='xy', scale=1, **kwargs)
    return ax


def spectrum(x, y, ax=None):
    pass


def spectrum_and_local_slope(x_spectrum, y_spectrum, x_slope, y_slope, ax_spectrum=None, ax_slope=None):
    pass


# Draw a brace over an area of len 'len_x'
def brace(len_x):
    y = numpy.concatenate(([0], numpy.ones(len_x - 2), [0]))
    return y
