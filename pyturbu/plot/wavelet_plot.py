#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: slion
# @Date:   2015-01-28 14:40:04
# @Last Modified by:   slion
# @Last Modified time: 2016-02-04 15:34:32

import numpy
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import pyturbu.tools.utility as utility


def phase_contour(ax, time, freqs, phase, freqs_coi, timedelta_dt=None):
    if timedelta_dt is None:
        timedelta_dt = time[1] - time[0]
    cf = ax.contourf(time,  freqs,   phase, [-195, -165, -135, -105, -75, -45, -15, 15, 45, 75, 105, 135, 165, 195], cmap=utility.phase_color_map(13))
    ax.set_ylim([max([freqs_coi.min(),  freqs.min()]), freqs.max()])
    ax.fill_between(numpy.concatenate([time[:1] - timedelta_dt,  time,  time[-1:] + timedelta_dt]),
                    1e-9,
                    numpy.concatenate([[1e-9],  freqs_coi,  [1e-9]]),  color='k',  alpha=0.3,  hatch='x')
    ax.set_yscale('log')
    cbar = plt.colorbar(cf, ticks=[-180, -90, 0, 90, 180])
    return cf, cbar


def phase_colormesh(ax, time, freqs, phase, freqs_coi, timedelta_dt=None, cb_label=r'$\Delta\phi_{xy}$', simplification_factor=4):
    if timedelta_dt is None:
        timedelta_dt = time[1] - time[0]
    colormesh = ax.pcolormesh(mdates.date2num(time), freqs, phase, cmap=utility.phase_color_map(), vmin=-180,  vmax=180)
    colormesh.set_rasterized(True)  # Bitmap pour pouvoir sortir les fichiers pdf plus vite
    # ax.set_ylim([max([freqs_coi.min(),  freqs.min()]), freqs.max()])
    # ax.fill_between(numpy.concatenate([time[:1]-timedelta_dt,  time,  time[-1:]+timedelta_dt]), \
    #   1e-9, \
    #   numpy.concatenate([[1e-9],  freqs_coi,  [1e-9]]),  color='k',  alpha = 0.3,  hatch='x')
    ax.set_yscale('log')
    cbar = plt.colorbar(colormesh, ticks=[-180, -90, 0, 90, 180])
    cbar.set_label(cb_label)


def spectrogram_colormesh(ax, time, freqs, power, freqs_coi, cb_label=r'$|W|^2$', cax=None, ticks=None, **kwargs):
    timedelta_dt = time[1] - time[0]
    log10_power = numpy.log10(power)
    colormesh = ax.pcolormesh(mdates.date2num(time),
                              freqs, log10_power, **kwargs)
    colormesh.set_rasterized(True)  # Bitmap pour pouvoir sortir les fichiers pdf plus vite
    ax.set_xlim([time.min(), time.max()])
    ax.set_yscale('log')
    ax.set_ylim([freqs.max(), max([freqs_coi.min(), freqs.min()])])
    ax.fill_between(numpy.concatenate([time[:1] - timedelta_dt, time, time[-1:] + timedelta_dt]),
                    1e-9,
                    numpy.concatenate([[1e-9], freqs_coi, [1e-9]]), color='k', alpha=0.3, hatch='x')
    if ticks is None:
        cbar = plt.colorbar(colormesh, cax=cax)
    else:
        cbar = plt.colorbar(colormesh, ticks=ticks, cax=cax)
    cbar.set_label(cb_label)
    # cbar.ax.get_xticklabels()[0].set_visible(True)


def spectrogram_contour(ax, time, freqs, power, freqs_coi, timedelta_dt=None, nlevel=10):
    if timedelta_dt is None:
        timedelta_dt = time[1] - time[0]
    ax.contourf(time,  freqs,  numpy.log10(power),  nlevel,
                extend='both')
    ax.fill_between(numpy.concatenate([time[:1] - timedelta_dt,  time,  time[-1:] + timedelta_dt]),
                    1e-9,
                    numpy.concatenate([[1e-9],  freqs_coi,  [1e-9]]),  color='k',  alpha=0.3,  hatch='x')

    ax.set_yscale("log")
    ax.set_ylim([max([freqs_coi.min(),  freqs.min()]), freqs.max()])
