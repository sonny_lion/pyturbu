#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: slion
# @Date:   2015-01-29 15:32:11
# @Last Modified by:   slion
# @Last Modified time: 2016-02-04 15:45:49

import numpy
import datetime

import pyturbu.tools.utility as utility
import pyturbu.read.probe_data as probe_data
import pyturbu.pycwt.wav as wavelet


def wavelet_coherence(start_Epoch, end_Epoch,
                      used_probe="WIND", download=False,
                      start_interval=0,
                      end_interval=0,
                      n_random=2, component="BxV,BxBxV", use_cache=False):

    if start_interval == 0:
        start_interval = start_Epoch+datetime.timedelta(minutes=10)
    if end_interval == 0:
        end_interval = end_Epoch-datetime.timedelta(minutes=10)

    # Version vectorise de la fonction total_seconds

    # ---- Load data ---- #

    save_filename = "_"+used_probe+start_interval.strftime('_%Y%m%d-%H%M%S')+end_interval.strftime('-%H%M%S_')+component.replace(",", "_")+"_nrand"+str(n_random)

    cache_loaded = use_cache

    probe_d = probe_data.Probe_data(used_probe, start_Epoch, end_Epoch, download=download)  # Variable dictionnaire des donnees de la sonde demandee

    time_interval = numpy.logical_and(probe_d.MAG_LF["Epoch"] >= start_interval, probe_d.MAG_LF["Epoch"] <= end_interval)  # Intervalle de temps a utiliser

    probe_d.BV_frame()  # Passage en B BV BBV frame

    # Recuperation de B ref (B,BxV,BxBxV)

    # Computing between x_var and y_var

    if component == "B,BxV":
        x_var, y_var, z_var = probe_d.get_lf_magnetic_data()
    elif component == "BxV,BxBxV":
        z_var, x_var, y_var = probe_d.get_lf_magnetic_data()
    elif component == "BxBxV,B":
        y_var, z_var, x_var = probe_d.get_lf_magnetic_data()
    else:
        print('Error, no good component selected: Exit()')
        return 1

    delta_seconds_flt = utility.vec_total_seconds(probe_d.MAG_LF["Epoch"]-probe_d.MAG_LF["Epoch"][0])  # Conversion du temps en float

    WCT, cwt1, cwt2, time, coi, freqs, aWCT = wavelet.sonny_wct(delta_seconds_flt, x_var, delta_seconds_flt, y_var,
                                                                significance_level=0.8646, normalize=True)

    m_wct, n_wct = WCT.shape
    mean_signif = numpy.zeros([m_wct, n_wct])  # Mean for random phases coherence
    std_signif = numpy.zeros([m_wct, n_wct])  # Standard Deviation for random phases coherence

    # Determine threshold for coherence
    if use_cache:
        print("Try to load cached data ...\n")
        cache_file = './Cache/cache_file'+save_filename+'.npz'
        try:
            cache_file_data = numpy.load(cache_file)
            std_signif = cache_file_data["std_signif"]
            mean_signif = cache_file_data["mean_signif"]
            print("Loading successful\n")
        except Exception, e:
            print(e)
            print("Error during cache loading")
            print("Computing new random data ...\n")
            cache_loaded = False
    if not cache_loaded:
        for x_bin in xrange(0, n_random):
            random_B1 = utility.randomize(x_var)
            random_B2 = utility.randomize(y_var)

            # Calculate the wavelet coherence (WTC). The WTC finds regions in time
            # frequency space where the two time seris co-vary, but do not necessarily have
            # high power.
            signif_part, wbin1, wbin2, time, coi, freqs, aWCT = wavelet.sonny_wct(delta_seconds_flt, random_B1, delta_seconds_flt, random_B2, significance_level=0.8646, normalize=True)
            mean_signif += signif_part
            std_signif += (signif_part*signif_part)
        mean_signif /= float(n_random)
        std_signif /= float(n_random)
        std_signif = numpy.sqrt(std_signif-mean_signif*mean_signif)
        if use_cache:
            numpy.savez(cache_file, mean_signif=mean_signif, std_signif=std_signif)
            print("New cache file saved at:", cache_file)

    return WCT, cwt1, cwt2,\
        probe_d, freqs, coi,\
        mean_signif, std_signif, time_interval
