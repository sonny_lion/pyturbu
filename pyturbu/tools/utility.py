import h5py    # HDF5 support
import numpy   # in this case, provides data structures
import math
import geometry
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.pyplot as plt
import matplotlib.dates
import scipy.interpolate
import scipy.ndimage
import datetime
import sys
# Find the highest power of two less than or equal to the input.


def dlProgress(count, blockSize, totalSize):
    percent = int(count*blockSize*100/totalSize)
    sys.stdout.write("%2d%%" % percent)
    sys.stdout.write("\b\b\b")
    sys.stdout.flush()


def randomize(signal):
    nb_tot = signal.size
    if (nb_tot % 2) == 0:
        fft_signal = numpy.fft.rfft(signal)
    else:
        fft_signal = numpy.fft.rfft(numpy.concatenate([signal, [signal[-1]]]))
    len_fft_signal = len(fft_signal)

    new_phase = 2.*numpy.pi*numpy.random.rand(len_fft_signal-1)

    exp_phase = numpy.zeros(len_fft_signal, dtype=complex)
    exp_phase[0] = 1.
    exp_phase[1:] = numpy.exp(1.0j*new_phase[:])

    return numpy.fft.irfft(numpy.abs(fft_signal)*exp_phase)[:nb_tot]


def grayscale_palette():
    colormap = [(207., 207., 207.),
                (165., 172., 175.),
                (143., 135., 130.),
                (96., 99., 106.),
                (65., 68., 81.)]
    for i in range(len(colormap)):
        r, g, b = colormap[i]
        colormap[i] = (r / 255., g / 255., b / 255.)
    return colormap


def latexify(rows=1, columns=1):

    # set plot attributes
    fig_width = 4*columns  # width in inches
    fig_height = 3*rows  # height in inches
    fig_size = [fig_width, fig_height]
    params = {'backend': 'Agg',
              'axes.labelsize': 8,
              'axes.titlesize': 8,
              'font.size': 8,
              'xtick.labelsize': 8,
              'ytick.labelsize': 8,
              'figure.figsize': fig_size,
              'savefig.dpi': 600,
              'font.family': 'sans-serif',
              'axes.linewidth': 0.5,
              'xtick.major.size': 2,
              'ytick.major.size': 2,
              'font.size': 8,
              'text.usetex': False,
              'mathtext.default': 'regular'
              }
    matplotlib.rcParams.update(params)


def lepow2(x):
    return numpy.int_(math.floor(numpy.log2(x)))


def time_div(time1, time2, sample):
    delta_t = time1-time2
    ind = 0
    while (delta_t > (ind*sample)):
        ind += 1
    return ind


def rebin2d(a, shape):
    sh = shape[0], a.shape[0]//shape[0], shape[1], a.shape[1]//shape[1]
    return a.reshape(sh).mean(-1).mean(1)


def congrid(a, newdims, method='linear', centre=False, minusone=False):
    '''Arbitrary resampling of source array to new dimension sizes.
    Currently only supports maintaining the same number of dimensions.
    To use 1-D arrays, first promote them to shape (x,1).

    Uses the same parameters and creates the same co-ordinate lookup points
    as IDL''s congrid routine, which apparently originally came from a VAX/VMS
    routine of the same name.

    method:
    neighbour - closest value from original data
    nearest and linear - uses n x 1-D interpolations using
                         scipy.interpolate.interp1d
    (see Numerical Recipes for validity of use of n 1-D interpolations)
    spline - uses ndimage.map_coordinates

    centre:
    True - interpolation points are at the centres of the bins
    False - points are at the front edge of the bin

    minusone:
    For example- inarray.shape = (i,j) & new dimensions = (x,y)
    False - inarray is resampled by factors of (i/x) * (j/y)
    True - inarray is resampled by(i-1)/(x-1) * (j-1)/(y-1)
    This prevents extrapolation one element beyond bounds of input array.
    '''
    if not a.dtype in [numpy.float64, numpy.float32]:
        a = numpy.cast[float](a)

    m1 = numpy.cast[int](minusone)
    ofs = numpy.cast[int](centre) * 0.5
    old = numpy.array(a.shape)
    ndims = len(a.shape)
    if len(newdims) != ndims:
        print "[congrid] dimensions error. " \
            "This routine currently only support " \
            "rebinning to the same number of dimensions."
        return None
    newdims = numpy.asarray(newdims, dtype=float)
    dimlist = []

    if method == 'neighbour':
        for i in range(ndims):
            base = numpy.indices(newdims)[i]
            dimlist.append((old[i] - m1) / (newdims[i] - m1)
                           * (base + ofs) - ofs)
        cd = numpy.array(dimlist).round().astype(int)
        newa = a[list(cd)]
        return newa

    elif method in ['nearest', 'linear']:
        # calculate new dims
        for i in range(ndims):
            base = numpy.arange(newdims[i])
            dimlist.append((old[i] - m1) / (newdims[i] - m1)
                           * (base + ofs) - ofs)
        # specify old dims
        olddims = [numpy.arange(i, dtype=numpy.float) for i in list(a.shape)]

        # first interpolation - for ndims = any
        mint = scipy.interpolate.interp1d(olddims[-1], a, kind=method)
        newa = mint(dimlist[-1])

        trorder = [ndims - 1] + range(ndims - 1)
        for i in range(ndims - 2, -1, -1):
            newa = newa.transpose(trorder)

            mint = scipy.interpolate.interp1d(olddims[i], newa, kind=method)
            newa = mint(dimlist[i])

        if ndims > 1:
            # need one more transpose to return to original dimensions
            newa = newa.transpose(trorder)

        return newa
    elif method in ['spline']:
        oslices = [slice(0, j) for j in old]
        oldcoords = numpy.ogrid[oslices]
        nslices = [slice(0, j) for j in list(newdims)]
        newcoords = numpy.mgrid[nslices]

        newcoords_dims = range(numpy.rank(newcoords))
        # make first index last
        newcoords_dims.append(newcoords_dims.pop(0))
        newcoords_tr = newcoords.transpose(newcoords_dims)
        # makes a view that affects newcoords

        newcoords_tr += ofs

        deltas = (numpy.asarray(old) - m1) / (newdims - m1)
        newcoords_tr *= deltas

        newcoords_tr -= ofs

        newa = scipy.ndimage.map_coordinates(a, newcoords)
        return newa
    else:
        print "Congrid error: Unrecognized interpolation type.\n", \
            "Currently only \'neighbour\', \'nearest\',\'linear\',", \
            "and \'spline\' are supported."
        return None


def flt2str(flt, rd=4):
    try:
        return ("%."+str(rd)+"g") % flt
    except TypeError:
        str_arr = numpy.empty(len(flt), dtype='S10')
        for idx, x in enumerate(flt):
            str_arr[idx] = (("%."+str(rd)+"g") % x)
        return str_arr


def min_ref_frame(Brtn, B0=None, print_info=False):
    cov_Brtn = numpy.cov(Brtn)

    eigenvalues, eigenvectors = numpy.linalg.eig(cov_Brtn)
    eigenvalues = eigenvalues/numpy.amax(eigenvalues)
    if print_info:
        print(eigenvalues)
    maxindex = eigenvalues.argmax()
    minindex = eigenvalues.argmin()
    if print_info:
        print(maxindex, minindex)
    e_min = eigenvectors[:, minindex]
    e_max = eigenvectors[:, maxindex]
    if B0 == None:
        B0 = numpy.mean(Brtn, axis=1)
    align_with_B0 = numpy.vdot(e_min, B0)/geometry.norm(B0)
    print("align with:", align_with_B0)
    if align_with_B0 < 0:
        e_min = (-1.0)*e_min
    if print_info:
        print(eigenvalues)
    e_med = geometry.cross_product(e_min, e_max)
    if print_info:
        print([e_max, e_med, e_min])
    for idx, x in enumerate(eigenvalues):
        if (minindex == idx):
            value_min = x
        elif (maxindex == idx):
            value_max = x
        else:
            value_med = x

    new_eigenvectors = numpy.empty((3, 3))
    new_eigenvectors[:, 0] = e_max
    new_eigenvectors[:, 1] = e_med
    new_eigenvectors[:, 2] = e_min
    # numpy.linalg.inv
    return new_eigenvectors


def window_smooth(x, window_len=11, window='hanning', mode='valid'):
    """smooth the data using a window with requested size.

    http://wiki.scipy.org/Cookbook/SignalSmooth

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal 
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.

    input:
        x: the input signal 
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal

    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)

    see also: 

    numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
    scipy.signal.lfilter

    TODO: the window parameter could be the window itself if an array instead of a string
    NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of just y.
    """
    if x.ndim != 1:
        raise ValueError, "smooth only accepts 1 dimension arrays."
    if x.size < window_len:
        raise ValueError, "Input vector needs to be bigger than window size."
    if window_len < 3:
        return x
    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"
    s = numpy.r_[2*x[0]-x[window_len-1::-1], x, 2*x[-1]-x[-1:-window_len:-1]]
    if window == 'flat':  # moving average
        w = numpy.ones(window_len, 'd')
    else:
        w = eval('numpy.'+window+'(window_len)')
    y = numpy.convolve(w/w.sum(), s, mode='same')
    return y[window_len:-window_len+1]


def fileName(text):
    import os
    my_fileName, my_fileExtension = os.path.splitext(os.path.basename(text))
    return my_fileName


def fileExtension(text):
    import os
    my_fileName, my_fileExtension = os.path.splitext(text)
    return my_fileExtension


def phase_color_map(nb=256):
    my_cdict = {'red': ((0.0, 1.0, 1.0),
                        (0.25, 0.0, 0.0),
                        (0.5, 0.0, 0.0),
                        (0.75, 1.0, 1.0),
                        (1.0, 1.0, 1.0)),
                'green': ((0.0, 1.0, 1.0),
                          (0.25, 0.0, 0.0),
                          (0.5, 0.0, 0.0),
                          (0.75, 0.0, 0.0),
                          (1.0, 1.0, 1.0)),
                'blue': ((0.0, 1.0, 1.0),
                         (0.25, 1.0, 1.0),
                         (0.5, 0.0, 0.0),
                         (0.75, 0.0, 0.0),
                         (1.0, 1.0, 1.0))}

    my_cmap = LinearSegmentedColormap('phase_cmap', my_cdict, nb)
    return my_cmap


def color_map_hsv():

    # 0     Red
    # 30    Orange
    # 60    Yellow
    # 90    Chartreuse green
    # 120   Green
    # 150   Spring green
    # 180   Cyan
    # 210   Azure
    # 240   Blue
    # 270   Violet
    # 300   Magenta
    # 330   Rose

    my_cmap = plt.cm.hsv
    return my_cmap


def plot_RTN(data_instr, start_Epoch=None, end_Epoch=None, component_list=None):
    #fig = plt.figure(figsize=(14, 12), dpi=100)
    fig, ax_array = plt.subplots(len(data_instr)-1, sharex=True, figsize=(14, 12), dpi=100)
    color_component = ["blue", "red", "green", "purple", "orange", "pink", "brown"]
    idx_component = 0
    if (component_list == None):
        for component in data_instr:
            if component != "Epoch":
                ax_array[idx_component].plot(matplotlib.dates.date2num(data_instr["Epoch"]), data_instr[component], color=color_component[idx_component])
                ax_array[idx_component].xaxis_date()
                ax_array[idx_component].xaxis.set_major_locator(matplotlib.dates.MinuteLocator(byminute=range(0, 60, 10)))
                ax_array[idx_component].xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%H:%M:%S'))
                ax_array[idx_component].locator_params(nbins=3, axis='y')
                ax_array[idx_component].set_ylabel(component)
                idx_component += 1
    else:
        for component in component_list:
            if component != "Epoch":
                ax_array[idx_component].plot(matplotlib.dates.date2num(data_instr["Epoch"]), data_instr[component], color=color_component[idx_component])
                ax_array[idx_component].xaxis_date()
                ax_array[idx_component].xaxis.set_major_locator(matplotlib.dates.MinuteLocator(byminute=range(0, 60, 10)))
                ax_array[idx_component].xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%H:%M:%S'))
                ax_array[idx_component].locator_params(nbins=3, axis='y')
                ax_array[idx_component].set_ylabel(component+' [nT]')
                idx_component += 1


def update_lines(num, dataLines, lines):
    for line, data in zip(lines, dataLines):
        # NOTE: there is no .set_data() for 3 dim data...
        line.set_data(data[0:2, :num])
        line.set_3d_properties(data[2, :num])
    return lines


def animated_RTN(data):
    """
    A simple example of an animated plot... In 3D!
    """
    import numpy as np
    import matplotlib.pyplot as plt
    import mpl_toolkits.mplot3d.axes3d as p3
    import matplotlib.animation as animation

    fig = plt.figure(figsize=(14, 12), dpi=100)
    ax = p3.Axes3D(fig)

    # Creating fifty line objects.
    # NOTE: Can't pass empty arrays into 3d version of plot()
    num_Epoch = matplotlib.dates.date2num(data["Epoch"])
    my_data = [numpy.array([num_Epoch, data["Bmed"], data["Bmax"]])]
    print(my_data)
    lines = [ax.plot(dat[0, 0:1], dat[1, 0:1], dat[2, 0:1])[0] for dat in my_data]

    # Setting the axes properties

    ax.set_xlim3d([num_Epoch.min(), num_Epoch.max()])
    ax.set_xlabel('X')
    ax.xaxis.set_major_locator(matplotlib.dates.MinuteLocator(byminute=range(0, 60, 10)))
    ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%H:%M:%S'))
    fig.autofmt_xdate()

    ax.set_ylim3d([data["Bmed"].min(), data["Bmed"].max()])
    ax.set_ylabel('Y')

    ax.set_zlim3d([data["Bmax"].min(), data["Bmax"].max()])
    ax.set_zlabel('Z')

    ax.set_title('3D Test')
    ax.plot(my_data[0][0, 0:], my_data[0][1, 0:], my_data[0][2, 0:], color="green", alpha=0.5)

    # Creating the Animation object
    line_ani = animation.FuncAnimation(fig, update_lines, len(data["Bmed"]), fargs=(my_data, lines),
                                       interval=50, blit=False)

    plt.show()


def addAttributes(parent, attr):
    """
    add attributes to an h5py data item

    :param obj parent: h5py parent object
    :param dict attr: dictionary of attributes
    """
    if attr and type(attr) == type({}):
        # attr is a dictionary of attributes
        for k, v in attr.items():
            parent.attrs[k] = v


def makeFile(filename, data_dict):
    """
    create and open an HDF5 file using h5py and the dictionary

    Any named parameters in the call to this method will be saved as
    attributes of the root of the file.
    Note that **attr is a dictionary of named parameters.

    :param str filename: valid file name
    :param data_dict: data
    :return: h5py file object
    """
    hdf5_file = h5py.File(filename, "w")
    print(data_dict.items())
    if data_dict and type(data_dict) == type({}):
        for key, value in data_dict.items():
            try:
                dset = hdf5_file.create_dataset(key, data=numpy.asarray(value))
            except TypeError, e:
                if type(value) is not datetime.datetime:
                    dset = hdf5_file.create_dataset(key, data=numpy.asarray([Epoch.isoformat("/") for Epoch in value]))
                else:
                    dset = hdf5_file.create_dataset(key, data=numpy.asarray(value.isoformat("/")))
    # addAttributes(hdf5_file, attr)
    return hdf5_file


def logical_and(arrays):
    return numpy.logical_and.reduce(arrays)


def logical_or(arrays):
    return numpy.logical_or.reduce(arrays)


# Fonction de conversion d'objet datetime.timedelta en nombre de secondes (float)
@numpy.vectorize
def vec_total_seconds(dt):
    return dt.total_seconds()
