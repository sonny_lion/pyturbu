#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: slion
# @Date:   2015-12-09 13:01:05
# @Last Modified by:   slion
# @Last Modified time: 2016-02-05 17:33:26

from scipy.optimize import curve_fit, fmin
from scipy.stats import genextreme, norm, dweibull
from scikits.statsmodels.tools.tools import ECDF
from sklearn.neighbors.kde import KernelDensity
import numpy
from scipy.special import k1
from leastsqbound import leastsqbound


def bandwidth(obs):
    return numpy.power(len(obs), -1./5.)*numpy.std(obs)*1.06  # https://en.wikipedia.org/wiki/Kernel_density_estimation


def increments(a, s=1):
    return a[s:]-a[:(len(a)-s)]


def kernel_estimation(obs, bw=None, n_points=1000, kernel='gaussian'):
    if bw is None:
        bw = bandwidth(obs)
    kde = KernelDensity(kernel=kernel, bandwidth=bw).fit(obs[:, numpy.newaxis])
    obs_min, obs_max = min(obs), max(obs)
    obs_x = numpy.linspace(obs_min, obs_max, n_points)[:, numpy.newaxis]
    log_dens = kde.score_samples(obs_x)
    epdf = numpy.exp(log_dens)
    ecdf = ECDF(epdf)
    return obs_x[:, 0], epdf, ecdf, bw


def gen_fit(func, x, y, maxfev=10000):
    popt, pcov = curve_fit(func, x, y, maxfev=maxfev)
    return func(x, *popt), popt, pcov


def gen_bound_fit(func, bounds_func, x, y, p0=[0.53396391, -0.32756256,  0.02197139,  1.27820852]):
    bounds = bounds_func()

    def err(p, y, x):
        return y - func(x, *p)

    p, cov_x, infodic, mesg, ier = leastsqbound(err, p0, args=(y, x),
                                                bounds=bounds, full_output=True)

    return func(x, *p), p, cov_x


def dweibull_fit(x, c, scale):
    return dweibull.pdf(x, c, loc=0.0, scale=scale)


def norm_fit(x, loc, scale):
    return norm.pdf(x, loc=loc, scale=scale)


def genextreme_fit(x, c, loc, scale):
    return genextreme.pdf(x, c, loc=loc, scale=scale)


def NIG_fit(x, alpha, beta, mu, delta):  # Normal-inverse Gaussian distribution
    # alpha*delta *K1(alpha*sqrt(delta*delta+(x-mu)^2))*exp([delta*sqrt(alpha*alpha - beta*beta) + beta*(x-mu)])/(pi*sqrt(delta*delta + (x-mu)^2))
    # mu location (R)
    # alpha_true tail heaviness (0>=|beta|>alpha_true) alpha_true = |beta| + |alpha|
    # beta asymmetry parameter (R)
    # delta scale parameter (R+)

    #  # Bound
    alpha_true = numpy.abs(beta) + numpy.abs(alpha)
    delta_true = numpy.abs(delta)
    gamma = numpy.sqrt(alpha_true*alpha_true - beta*beta)
    y = numpy.sqrt(delta_true*delta_true + (x-mu)*(x-mu))
    return alpha_true*delta_true*k1(alpha_true*y)/(numpy.pi*y)*numpy.exp(delta_true*gamma+beta*(x-mu))


def NIG_bounds():
    return [(0, None), (None, None), (None, None), (0, None)]


def NIG(x, alpha, beta, mu, delta):
    return alpha*delta * k1(alpha*numpy.sqrt(delta*delta+(x-mu)**2))*numpy.exp(delta*numpy.sqrt(alpha*alpha - beta*beta) + beta*(x-mu)) / (numpy.pi*numpy.sqrt(delta*delta + (x-mu)**2))


def NIG_bis(p, x):
    gamma, beta, mu, delta = p
    alpha_true = numpy.sqrt(beta*beta + gamma*gamma)
    delta_true = numpy.abs(delta)
    gamma = numpy.sqrt(alpha_true*alpha_true - beta*beta)
    y = numpy.sqrt(delta_true*delta_true + (x-mu)*(x-mu))
    return alpha_true*delta_true*k1(alpha_true*y)/(numpy.pi*y)*numpy.exp(delta_true*gamma+beta*(x-mu))


def random_NIG(n, alpha=1, beta=0, mu=0, delta=1):
    # A function implemented by Sonny Lion
    # Adapted from the R implementation by Diethelm Wuertz - https://cran.r-project.org/web/packages/fBasics/
    # Ref. Lévy Processes in Finance: Theory, Numerics, and Empirical Facts - pg 145 - S. Raible (2000)

    # Description:
    #   Return normal inverse Gaussian distributed random variates

    # Arguments:
    #   n - number of deviates to be generated
    #   alpha, beta - Shape Parameter, |beta| <= alpha
    #   delta  - Scale Parameter, 0 <= delta
    #   mu - Location Parameter

    # FUNCTION:

    # Settings:
    gamma = numpy.sqrt(alpha*alpha - beta*beta)

    # GAMMA:
    if gamma == 0:
        # GAMMA = 0:
        V = numpy.random.randn(n)**2
        Z = delta*delta / V
        X = numpy.sqrt(Z)*numpy.random.randn(n)
    else:
        # GAMMA > 0:
        U = numpy.random.random_sample(n)
        V = numpy.random.randn(n)**2
        # FIXED ...

        def z1(v, delta, gamma):
            return delta/gamma + v/(2*gamma**2) - numpy.sqrt(v*delta/(gamma**3) + (v/(2*gamma**2))**2)

        def z2(v, delta, gamma):
            return (delta/gamma)**2 / z1(v, delta, gamma)

        def pz1(v, delta, gamma):
            return delta / (delta + gamma * z1(v, delta, gamma))

        s = (1-numpy.sign(U-pz1(V, delta, gamma)))*0.5
        Z = z1(V, delta, gamma)*s + z2(V, delta, gamma)*(1-s)
        X = mu + beta*Z + numpy.sqrt(Z)*numpy.random.randn(n)

        return X


def abs_NIG(x, alpha, beta, delta):
    return NIG(x, alpha, beta, delta) + NIG(-x, alpha, beta, delta)


def histo(data, bins=None, bw=None):
    if bw is None:
        bw = bandwidth(data)
    if bins is None:
        bins = numpy.arange(min(data), max(data) + bw, bw)
    bins, edges = numpy.histogram(data, bins=bins, normed=True)
    left, right = edges[:-1], edges[1:]
    X = numpy.array([left, right]).T.flatten()
    Y = numpy.array([bins, bins]).T.flatten()
    return X, Y


def bootstrap_error_estimate(data, func, m=128):
    n = len(data)
    h = numpy.zeros(m)
    bootSamp = numpy.zeros(n)
    for sample in xrange(0, m):
        bootSamp = data[numpy.random.randint(n, size=n)]
        h[sample] = func(bootSamp)
    origEstim = func(data)
    resError = numpy.std(h)
    return origEstim, resError


def mleEstimate(func, p0, data):
    def lnL_av(x, p):
        return numpy.mean(numpy.log(func(p, x)))

    def objFunc(param):
        return -lnL_av(data, param)

    parameter_mle = fmin(objFunc, p0, disp=0)
    return parameter_mle
