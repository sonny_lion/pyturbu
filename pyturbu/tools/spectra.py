#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: slion
# @Date:   2015-01-29 15:50:54
# @Last Modified by:   slion
# @Last Modified time: 2016-01-22 15:36:13

import numpy
import scipy.stats as stats
from leastsqbound import leastsqbound


def cwt_spectrum_error(freq, psd, N, signif, dt, lambd=1.03, gamma=2.32):
    # freq - wavelet frequency array
    # psd - wavelet psd array
    # N - number of datapoints used to computed the psd
    # signif - significance level (example: signif=95 for 95% threshold)
    # dt - time sted of the data
    # lambd - conversion factor between scales and frequencies (1.03 for the Morlet wavelet)
    # gamma - decorrelation factor (2.32 for the Morlet wavelet)

    a = (100 - signif) * 1e-2
    b1 = a*0.5
    b2 = 1. - a*0.5
    tau = gamma/(lambd*freq)

    n = 2.*N*dt/tau

    bound1 = psd*n/stats.chi2.ppf(b1, n)
    bound2 = psd*n/stats.chi2.ppf(b2, n)
    return bound1, bound2


def linear_fit(freq, PSD, fmin_a, fmax_a, dictionary=True):

    # Regression analysis: http://en.wikipedia.org/wiki/Regression_analysis
    # slope, intercept, r, prob2, see = linregress(x, y)
    # mx = x.mean()
    # sx2 = ((x-mx)**2).sum()
    # sd_intercept = see * sqrt(1./len(x) + mx*mx/sx2)
    # sd_slope = see * sqrt(1./sx2)

    try:

        indice_a = numpy.logical_and(freq >= fmin_a, freq <= fmax_a)
        xa = numpy.log10(freq[indice_a])
        ya = numpy.log10(PSD[indice_a])

        alpha, a, r_value_a, p_value_a, std_err_a = stats.linregress(xa, ya)

        mxa = xa.mean()
        sxa2 = ((xa - mxa)**2).sum()
        err_a = std_err_a * numpy.sqrt(1. / len(xa) + mxa * mxa / sxa2)
        err_alpha = std_err_a * numpy.sqrt(1. / sxa2)
        if dictionary:
            return dict(slope=alpha, err_slope=err_alpha,
                        intercept=a, err_intercept=err_a)
        else:
            return alpha, a, err_alpha, err_a

    except Exception, e:
        print(e)
    if dictionary:
        return dict(slope=numpy.nan, err_slope=numpy.nan,
                    intercept=numpy.nan, err_intercept=numpy.nan)
    else:
        return numpy.nan, numpy.nan, numpy.nan, numpy.nan


def linear_fit_all(freqs, PSD, fmin, fmax, n_step=12, dictionary=True):
    fmin_a = max([fmin, freqs.min()])
    fmax_a = min([fmax, freqs.max()])

    freqs_fit = numpy.logspace(
        numpy.log10(fmin_a), numpy.log10(fmax_a), n_step)

    result = numpy.array([numpy.asarray(linear_fit(freqs, PSD, freqs_fit[i_freq - 1], freqs_fit[i_freq + 1], dictionary=False))
                          for i_freq in xrange(1, n_step - 1)])

    if dictionary:
        return dict(slope=result[:, 0], err_slope=result[:, 2],
                    intercept=result[:, 1], err_intercept=result[:, 3], freqs=freqs_fit[1:-1])
    else:
        return result[:, 0], result[:, 1], result[:, 2], result[:, 3], freqs_fit[1:-1]


def exponential_fit_func(p, f):
    alpha0, e0, f0 = p
    return (numpy.log(e0) + alpha0 * numpy.log(f) - f / f0)  # Target function


def exponential_fit_err(p, y, x):
    return y - exponential_fit_func(p, x)


def exponential_fit(freq, PSD, fmin_a, fmax_a):
    indice_a = numpy.logical_and(freq >= fmin_a, freq <= fmax_a)
    xa = freq[indice_a]
    ya = numpy.log(PSD[indice_a])
    e0 = 1
    f0 = 1
    alpha0 = -5. / 3.

    p0 = (alpha0, e0, f0)  # Initial guess for the parameters
    bounds = [(-4.0, -1.0), (0.0, None), (0.001, 1.0)]

    try:
        p1, cov_x, infodic, mesg, ier = leastsqbound(exponential_fit_err, p0, args=(ya, xa),
                                                     bounds=bounds, full_output=True)
    except Exception, e:
        print(e)
        return (numpy.nan, numpy.nan, numpy.nan)
    return p1


def exponential_and_noise_fit_func(p, f):
    alpha0, e0, f0, e1 = p
    # Target function
    return numpy.log(e0 * f**alpha0 * numpy.exp(-f / f0) + e1 / f)


def exponential_and_noise_fit_err(p, y, x):
    return (y - exponential_and_noise_fit_func(p, x))


def exponential_and_noise_fit(freq, PSD, fmin_a, fmax_a):
    indice_a = numpy.logical_and(freq >= fmin_a, freq <= fmax_a)
    xa = freq[indice_a]
    ya = numpy.log(PSD[indice_a])
    e0 = 5.2e-2
    f0 = 3.4e-1
    e1 = 3.3e-4
    alpha0 = -1.5

    p0 = (alpha0, e0, f0, e1)  # Initial guess for the parameters
    bounds = [(-4.0, -1.0), (0.0, None), (0.001, 1.0), (0.0, None)]

    try:
        p1, cov_x, infodic, mesg, ier = leastsqbound(exponential_and_noise_fit_err,
                                                     p0, args=(ya, xa),
                                                     bounds=bounds,
                                                     full_output=True)
    except Exception, e:
        print(e)
        return (numpy.nan, numpy.nan, numpy.nan, numpy.nan)
    return p1
