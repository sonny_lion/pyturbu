import numpy
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

def cross_product(u,v):
    return [u[1]*v[2]-u[2]*v[1],
            u[2]*v[0]-u[0]*v[2],
            u[0]*v[1]-u[1]*v[0]]

def norm(u):
    u=numpy.array(u)
    return numpy.sqrt(numpy.sum(u*u))

def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / numpy.linalg.norm(vector)

def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    angle = numpy.arccos(numpy.dot(v1_u, v2_u))
    if numpy.isnan(angle):
        if (v1_u == v2_u).all():
            return 0.0
        else:
            return 180.0
    return numpy.rad2deg(angle)
def angle_between_array(a1, a2=None,component_list=["R","T","N"],component_list2=None):
    """ if a2==None return the angle between a1 and the radial direction (1st component)"""
    if a2==None:
        a1_norm = numpy.sqrt(a1[component_list[0]]*a1[component_list[0]] + a1[component_list[1]]*a1[component_list[1]] + a1[component_list[2]]*a1[component_list[2]])
        a1_u_1st_comp=a1[component_list[0]]/a1_norm
        angle = numpy.arccos(a1_u_1st_comp)
        return numpy.rad2deg(angle)
    if component_list2==None:
        component_list2=component_list
    a1_norm = numpy.sqrt(a1[component_list[0]]*a1[component_list[0]] + a1[component_list[1]]*a1[component_list[1]] + a1[component_list[2]]*a1[component_list[2]])
    a2_norm = numpy.sqrt(a2[component_list2[0]]*a2[component_list2[0]] +\
     a2[component_list2[1]]*a2[component_list2[1]] +\
      a2[component_list2[2]]*a2[component_list2[2]])
    a1_u={}
    a2_u={}
    for idx in [0,1,2]:
        a1_u[component_list[idx]]=a1[component_list[idx]]/a1_norm
        a2_u[component_list2[idx]]=a2[component_list2[idx]]/a2_norm
    angle = numpy.arccos(a1_u[component_list[0]]*a2_u[component_list2[0]] + a1_u[component_list[1]]*a2_u[component_list2[1]] + a1_u[component_list[2]]*a2_u[component_list2[2]])
    # if numpy.isnan(angle):
    #     if (v1_u == v2_u).all():
    #         return 0.0
    #     else:
    #         return 180.0
    return numpy.rad2deg(angle)

class Arrow3D(FancyArrowPatch):
    def __init__(self,start,stop, *args, **kwargs):
    	xs=[start[0],stop[0]]
    	ys=[start[1],stop[1]]
    	zs=[start[2],stop[2]]
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)

