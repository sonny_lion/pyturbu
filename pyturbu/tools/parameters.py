#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: slion
# @Date:   2015-03-10 11:14:31
# @Last Modified by:   slion
# @Last Modified time: 2015-03-10 11:21:49

import numpy


def mass_proton():
    return 1.672622 * 1.0e-27  # kg


def epsilon_0():
    return 8.85418782 * 1.0e-12


def mu_0():
    return 4.0 * numpy.pi * 1e-7


def charge_proton():
    return 1.602176565 * 1.0e-19


def light_speed():
    return 299792458.0


def two_pi():
    return 2.0 * numpy.pi


def Boltzmann_const():
    return 1.3806488 * 1e-23


def ion_thermal_velocity(Tp):
    return numpy.sqrt(2.0 * Boltzmann_const() * Tp / mass_proton())


def ion_temperature(Vth):
    return (Vth * Vth) * 0.5 * mass_proton() / Boltzmann_const()


def ion_cyclotron_frequency(B):
    return charge_proton() * B / (two_pi() * mass_proton())  # Hz


def Larmor_radius(Tp, B):
    return mass_proton() * ion_thermal_velocity(Tp) / (charge_proton() * B)


def plasma_frequency(Np):
    return (charge_proton() * numpy.sqrt(Np)) / numpy.sqrt(mass_proton() * epsilon_0())


def inertial_length(Np):
    return light_speed() / plasma_frequency(Np)


def Alfven_velocity(B, N):
    return B / numpy.sqrt(mu_0() * N * mass_proton())


def beta(n, T, B):
    up = n * Boltzmann_const() * T
    down = (B * B * 0.5 / mu_0())
    return up / down
