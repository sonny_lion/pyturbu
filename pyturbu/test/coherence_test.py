#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: slion
# @Date:   2015-01-29 15:42:06
# @Last Modified by:   slion
# @Last Modified time: 2015-01-29 17:25:32
import datetime
import pyturbu.tools.coherence as cohetools

toto=cohetools.wavelet_coherence(datetime.datetime(1995,1,30,12,50),datetime.datetime(1995,1,30,14,10))
print(toto)