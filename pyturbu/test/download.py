#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: slion
# @Date:   2015-01-29 12:43:07
# @Last Modified by:   slion
# @Last Modified time: 2015-01-29 14:14:40

from pyturbu.read.probe_data import Probe_data

probe_name="WIND"
my_probe=Probe_data(probe_name,"2009-03-30/13:00:00.0","2009-03-30/14:00:00.0",download=True)

print(my_probe)