#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: slion
# @Date:   2015-01-29 00:29:52
# @Last Modified by:   slion
# @Last Modified time: 2015-01-29 01:12:18

import matplotlib.pyplot as plt

import pyturbu.plot.cosmetic as cosmetic

cosmetic.mplstyle(["blues","colors","paper"])

plt.plot([1,2,3,4])

cosmetic.hide_spines()

plt.show()