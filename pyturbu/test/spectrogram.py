#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: slion
# @Date:   2015-01-29 00:30:05
# @Last Modified by:   slion
# @Last Modified time: 2015-01-29 00:58:41

import numpy
import datetime
import sys
import matplotlib.pyplot as plt

import pyturbu.pycwt.wav as wavelet
import pyturbu.plot.wavelet_plot as wavelet_plot
from pyturbu.read.probe_data import Probe_data

import pyturbu.plot.cosmetic as cosmetic

cosmetic.mplstyle(["blues","colors"])

#test_probe=Probe_data("WIND","1995-01-30/13:00:00.0","1995-01-30/14:00:00.0")
probe_name="STA"
plt.figure(figsize=[8,3])
my_probe=Probe_data(probe_name,"2008-01-30/13:00:00.0","2008-01-30/14:00:00.0")

Bx,By,Bz=my_probe.get_lf_magnetic_data()
dt_stb=my_probe.lf_mag_dt()

Bx_wave, scales, freqs, coi, Bx_fft, fftfreqs = wavelet.cwt(Bx, dt=dt_stb)

freqs_coi=1.0/coi
Bx_power= (abs(Bx_wave)) ** 2  

Bx_glbl_power = Bx_power.mean(axis=1)

ax_color_bar = plt.axes([0.1,0.1,0.01,0.8])
ax_spectrogram = plt.axes([0.17,0.1,0.5,0.8])
ax_spectrum = plt.axes([0.67,0.1,0.25,0.8],sharey=ax_spectrogram)
wavelet_plot.spectrogram_colormesh(ax_spectrogram,my_probe.MAG_LF["Epoch"],freqs,\
	Bx_power,freqs_coi,cax=ax_color_bar)
ax_spectrogram.set_xlabel(probe_name)
ax_spectrum.plot(Bx_glbl_power,freqs)
ax_spectrum.set_xscale("log")
ax_spectrum.set_yscale("log")


plt.show()