#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: Sonny
# @Date:   2015-01-26 21:13:41
# @Last Modified by:   slion
# @Last Modified time: 2015-10-16 15:01:48

# -----------------------------------------------------------------------------
# Filename:    stereo_cdf.py
# -----------------------------------------------------------------------------
#
# Sonny LION - sonny.lion@obspm.fr
# Doctorant - PhD Student
# Laboratoire d'Etudes Spatiales et d'Instrumentation en Astrophysique (LESIA)
# -----------------------------------------------------------------------------

from scipy.io import readsav
import numpy
import datetime


def seconds_to_epoch(sec):
    dt = datetime.timedelta(seconds=sec)
    return (datetime.datetime(2002, 2, 19) + dt)


def read(data_path="/home/slion/python/thesis/pyturbu/pyturbu/read/download/cluster/"):

    data_mag = readsav(data_path+"save_020219.sav")
    data_plasma = readsav(data_path+"plasma_par_new.sav")

    gg = 3600
    time_range_mask = numpy.logical_and(data_mag["time"] > 0.2 * gg, data_mag["time"] < 2.6 * gg)

    data =\
        dict(
            MAG_LF=dict(),
            Proton=dict(),
        )
    vec_seconds_to_epoch = numpy.vectorize(seconds_to_epoch)
    data["MAG_LF"]["Epoch"] = vec_seconds_to_epoch(data_mag["time"][time_range_mask])
    data["MAG_LF"]["Bx"] = data_mag["b_c1_hr"][time_range_mask][:, 0]
    data["MAG_LF"]["By"] = data_mag["b_c1_hr"][time_range_mask][:, 1]
    data["MAG_LF"]["Bz"] = data_mag["b_c1_hr"][time_range_mask][:, 2]

    data["Proton"]["Epoch"] = vec_seconds_to_epoch(data_mag["time"][time_range_mask])
    data["Proton"]["Vx"] = data_plasma["vv"][:, 0]
    data["Proton"]["Vy"] = data_plasma["vv"][:, 1]
    data["Proton"]["Vz"] = data_plasma["vv"][:, 2]
    data["Proton"]["T"] = data_plasma["vv"][:, 0]*0. + 1.
    data["Proton"]["T_perp"] = data_plasma["vv"][:, 0]*0. + 1.
    data["Proton"]["T_para"] = data_plasma["vv"][:, 0]*0. + 1.
    data["Proton"]["N"] = data_plasma["vv"][:, 0]*0. + 1.

    return data


if __name__ == '__main__':

    data = read()
    print(len(data["MAG_LF"]["Epoch"]))
    print(len(data["MAG_LF"]["Bx"]))
    print(len(data["Proton"]["Vx"]))
    exit()
