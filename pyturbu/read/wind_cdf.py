#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: Sonny
# @Date:   2015-01-26 21:13:41
# @Last Modified by:   slion
# @Last Modified time: 2015-01-29 14:22:32

# -----------------------------------------------------------------------------
# Filename:    wind_cdf.py
# -----------------------------------------------------------------------------  
#
# Sonny LION - sonny.lion@obspm.fr
# Doctorant - PhD Student
# Laboratoire d'Etudes Spatiales et d'Instrumentation en Astrophysique (LESIA)
# -----------------------------------------------------------------------------

import datetime
import numpy
import pandas
from spacepy import pycdf
import urllib
import os
import sys
import pyturbu.tools.parameters as parameters
import pyturbu.read
import pyturbu.tools.utility as utility


def read(input_start_Epoch, input_end_Epoch,data_path = "/home/slion/Data/wind/",download=False):

	# Exemple: read("1995-01-30/13:00:00.0","1995-01-30/14:00:00.0")
	# 
	# A changer: hard coded sample rate
	# 
	# Data structure used:
	# 
	# Data/
	#     MAG_LF/
	#         Bx
	#         By
	#         Bz
	#         Epoch
	#         Info/
	#             Instrument
	#             Version
	#             Ref_frame
	#             Unit
	#     Proton/
	#         Vx
	#         Vy
	#         Vz
	#         N
	#         T
	#         T_perp
	#         T_para
	#         Epoch
	#         Info/
	#             Instrument
	#             Version
	#             Ref_frame
	#             V_unit
	#             N_unit
	#             T_unit
	#     Probe
	# 


	Info_MFI = {'Instrument':"MFI",'Version': '05', 'Ref_frame': 'GSE',"Unit":"nT"}
	Info_WSE = {'Instrument':"WSE",'Version': '01', 'Ref_frame': 'GSE',"V_unit":"km/s","N_unit":"1/cc","T_unit":"K"}

	data=\
	 dict(\
	    MAG_LF = dict(\
	    	Info=Info_MFI),\
	    Proton = dict(\
	    	Info=Info_WSE),\
	    Probe = "WIND" ,\
	    )

	if type(input_start_Epoch) is datetime.datetime:
		start_Epoch=input_start_Epoch
	else:
		start_Epoch=datetime.datetime.strptime(input_start_Epoch,"%Y-%m-%d/%H:%M:%S.%f")
	if type(input_start_Epoch) is datetime.datetime:
		end_Epoch=input_end_Epoch
	else:
		end_Epoch=datetime.datetime.strptime(input_end_Epoch,"%Y-%m-%d/%H:%M:%S.%f")


	download_url="ftp://cdaweb.gsfc.nasa.gov/pub/data/wind/"
	download_path="%s/download/wind/" % os.path.dirname(pyturbu.read.__file__)

	# MFI Data

	MFI_filename=start_Epoch.strftime("wi_h2_mfi_%Y%m%d_v05.cdf")

	try:
		MFI_data=pycdf.CDF(data_path+MFI_filename)
	except Exception, e:
		print(e)
		print(data_path+MFI_filename)
		try:
			MFI_data=pycdf.CDF(download_path+MFI_filename)
		except Exception, e:
			print(e)
			print(download_path+MFI_filename)
			if download:
				try:
					print("\ntry to download file from cdaweb")
					MFI_url=start_Epoch.strftime("mfi/mfi_h2/%Y/")
					sys.stdout.write(MFI_filename + "...")
					urllib.urlretrieve(download_url+MFI_url+MFI_filename, download_path+MFI_filename,reporthook=utility.dlProgress)
					print("\nfile save at: %s%s" % (download_path, MFI_filename))
					try:
						MFI_data=pycdf.CDF(download_path+MFI_filename)
					except Exception, e:
						print(e)
						print("fail to load downloaded file\n")
						return None
				except Exception, e:
					print(e)
					print("fail to download file\n")
					return None
			else:
				print("file not found, try to active download in order to retrieve the selected file")
				return None

	datetime_index=pandas.DatetimeIndex(MFI_data["Epoch"][:,0])
	
	MFI_pandas_data=pandas.DataFrame({"Bx":MFI_data["BGSE"][:,0],"By":MFI_data["BGSE"][:,1],"Bz":MFI_data["BGSE"][:,2],},index=datetime_index)

	MFI_data.close()

	# WSE Data

	WSE_filename=start_Epoch.strftime("wi_h1_swe_%Y%m%d_v01.cdf")

	try:
		WSE_data=pycdf.CDF(data_path+WSE_filename)
	except Exception, e:
		print(e)
		print(data_path+WSE_filename)
		try:
			WSE_data=pycdf.CDF(download_path+WSE_filename)
		except Exception, e:
			print(e)
			print(download_path+WSE_filename)
			if download:
				try:
					print("\ntry to download file from cdaweb")
					WSE_url=start_Epoch.strftime("swe/swe_h1/%Y/")
					sys.stdout.write(WSE_filename + "...")
					urllib.urlretrieve(download_url+WSE_url+WSE_filename, download_path+WSE_filename,reporthook=utility.dlProgress)
					print("\nfile save at: %s%s" % (download_path, WSE_filename))
					try:
						WSE_data=pycdf.CDF(download_path+WSE_filename)
					except Exception, e:
						print(e)
						print("fail to load downloaded file\n")
						return None
				except Exception, e:
					print(e)
					print("fail to download file\n")
					return None
			else:
				print("file not found, try to active download in order to retrieve the selected file")
				return None
	
	WSE_datetime_index=pandas.DatetimeIndex(WSE_data["Epoch"][...])
	
	WSE_pandas_data=pandas.DataFrame({"V_x":WSE_data["Proton_VX_nonlin"][...],\
	"V_y":WSE_data["Proton_VY_nonlin"][...],\
	"V_z":WSE_data["Proton_VZ_nonlin"][...],\
	"V_thermal":WSE_data["Proton_W_nonlin"][...],\
	"V_thermal_per":WSE_data["Proton_Wperp_nonlin"][...],\
	"V_thermal_par":WSE_data["Proton_Wpar_nonlin"][...],\
	"Density":WSE_data["Proton_Np_nonlin"][...],},\
	index=WSE_datetime_index)

	WSE_data.close()

	all_pandas_data=pandas.merge(MFI_pandas_data,WSE_pandas_data,how='outer',left_index=True,right_index=True)
	all_pandas_data_index=all_pandas_data.index.to_pydatetime()
	good_index=numpy.logical_and(all_pandas_data_index>=datetime_index[0],all_pandas_data_index<=datetime_index[-1])
	resampled=all_pandas_data.interpolate("time")[good_index].resample('184L').interpolate()

	good_index=numpy.logical_and(resampled.index.to_pydatetime()>=start_Epoch,\
		resampled.index.to_pydatetime()<=end_Epoch)
	data["MAG_LF"]["Epoch"]=resampled.index.to_pydatetime()[good_index]
	data["MAG_LF"]["Bx"]=resampled['Bx'].values[good_index]
	data["MAG_LF"]["By"]=resampled['By'].values[good_index]
	data["MAG_LF"]["Bz"]=resampled['Bz'].values[good_index]

	data["Proton"]["Epoch"]=resampled.index.to_pydatetime()[good_index]
	data["Proton"]["Vx"]=resampled["V_x"].values[good_index]
	data["Proton"]["Vy"]=resampled["V_y"].values[good_index]
	data["Proton"]["Vz"]=resampled["V_z"].values[good_index]
	data["Proton"]["T"]=parameters.ion_temperature(resampled["V_thermal"][good_index]*1000.)
	data["Proton"]["T_perp"]=parameters.ion_temperature(resampled["V_thermal_per"][good_index]*1000.)
	data["Proton"]["T_para"]=parameters.ion_temperature(resampled["V_thermal_par"][good_index]*1000.)
	data["Proton"]["N"]=resampled["Density"].values[good_index]

	return data


if __name__ == '__main__':


	data=read("1995-01-30/13:00:00.0","1995-01-30/14:00:00.0")

	
	print(data)
	exit()


