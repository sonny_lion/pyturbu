
import pandas
import datetime
import numpy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def parse_doy(year, doy):
    return datetime.datetime(year, 1, 1) + datetime.timedelta(days=(doy-1))

class Csv_ephemeris:

    data = None
    filename = None

    def __init__(self, var_filename, data_path=""):

        parse = numpy.vectorize(lambda x: datetime.datetime.strptime(x, ' A.D. %Y-%b-%d %H:%M:%S.%f'))

        self.data = pandas.read_csv(data_path+var_filename, engine='python', names=["JDCT", "Epoch", "X", "Y", "Z", "VX", "VY", "VZ","VOID"],
                           usecols=["JDCT", "Epoch", "X", "Y", "Z", "VX", "VY", "VZ"],
                           delimiter=",",skiprows=23, skip_footer=34)
        self.data["Epoch"] = pandas.DatetimeIndex(parse(self.data["Epoch"]))
        self.data = self.data.set_index("Epoch")
        self.filename = var_filename
    def __getitem__(self, item):
        if item=="Epoch":
            return self.data.index
        return self.data[item]

    def __str__(self):
        return str(self.data)

    def compute(self, time_array):
        merged_data = pandas.concat([self.data, self.data.reindex(index=pandas.DatetimeIndex(time_array))], join='outer',)
        merged_data["index"] = merged_data.index
        merged_data.drop_duplicates(subset='index', take_last=True, inplace=True)
        merged_data = merged_data.drop('index', axis=1)
        merged_data = merged_data.sort()
        return merged_data.interpolate(method='time').reindex(index=time_array)

class Ascii_ephemeris:

    data = None
    filename = None

    def __init__(self, var_filename, data_path=""):

        vec_parse_doy = numpy.vectorize(parse_doy)

        au = 149597871.  # kilometers
        numpy_data = numpy.loadtxt(data_path+var_filename, skiprows=1)

        dict_data = {} 

        dict_data["X"] = numpy_data[:, 2] * au
        dict_data["Y"] = numpy_data[:, 3] * au
        dict_data["Z"] = numpy_data[:, 4] * au

        self.data = pandas.DataFrame(data=dict_data, index=pandas.DatetimeIndex(vec_parse_doy(numpy_data[:, 0].astype(int),numpy_data[:, 1].astype(int))))

        self.filename = var_filename
    def __getitem__(self, item):
        if item=="Epoch":
            return self.data.index
        return self.data[item]

    def __str__(self):
        return str(self.data)

    def compute(self, time_array):
        merged_data = pandas.concat([self.data, self.data.reindex(index=pandas.DatetimeIndex(time_array))], join='outer',)
        merged_data["index"] = merged_data.index
        merged_data.drop_duplicates(subset='index', take_last=True, inplace=True)
        merged_data = merged_data.drop('index', axis=1)
        merged_data = merged_data.sort()
        return merged_data.interpolate(method='time').reindex(index=time_array)

def plot_ephemeris(ax, start_Epoch, bodies, steps=90, delta_t=datetime.timedelta(days=1), with_alpha = True, data_path=""):

    # Parameters
    
    Epoch_array = start_Epoch + numpy.arange(steps) * delta_t

    # Get ephemeris

    key_to_remove =[]

    for key, value in bodies.iteritems():
        if key == "HELIOS":
            value["kernel"] = Ascii_ephemeris(key+"-SSB.txt", data_path=data_path)
        else:
            value["kernel"] = Csv_ephemeris(key+"-SSB.csv", data_path=data_path)
        value["eph"] = value["kernel"].compute(Epoch_array)
        if numpy.isnan(value["eph"]["X"]).all():
            key_to_remove.append(key)

    # Check value and remove bodies without data

    for key in key_to_remove:
        del bodies[key]
        print("No data for "+key+" - Removed")

    # Do the ploting ! 

    # Transparency for the trend

    if with_alpha:

        for part in xrange(0,10):
            min_idx = steps/10 * part
            if part+1 != 10:
                max_idx = steps/10 * (part+1) +1
            else:
                max_idx = steps

            for key, value in bodies.iteritems():
                ax.plot(value["eph"]["X"][min_idx:max_idx], value["eph"]["Y"][min_idx:max_idx], value["eph"]["Z"][min_idx:max_idx], color=value["color"], alpha=0.1*(part+1))

    # Or not

    else:
        for key, value in bodies.iteritems():
            ax.plot(value["eph"]["X"], value["eph"]["Y"], value["eph"]["Z"], color=value["color"])

    # A sphere for each body

    for key, value in bodies.iteritems():
        ax.scatter(value["eph"]["X"][-1], value["eph"]["Y"][-1], zs=value["eph"]["Z"][-1], s=20, color=value["color"], label=key[0]+key[1:].lower())

    # Limit and legend

    ax.set_xlim([-0.5e9, 0.5e9])
    ax.set_ylim([-1e9, 1e9])
    ax.set_zlim([-0.5e9, 0.5e9])
    ax.legend(scatterpoints=1)

if __name__ == '__main__':

    # Example

    delta_t = datetime.timedelta(days=1)
    number_days = 90
    with_alpha = True
    start_Epoch = datetime.datetime(1992,9,1)


    bodies = {"STB":{"color": "#9a3614"},
              "STA":{"color": "#474B4E"},
              "JUPITER":{"color": "#834B21"},
              "ULYSSES":{"color": "#8A2BE2"},
              "SUN":{"color": "#FFB42C"},
              "EARTH":{"color": "#3F6CB4"},
              }

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    plot_ephemeris(ax, start_Epoch, bodies, steps=number_days, delta_t=delta_t, data_path="./data/ephemeris/")
    plt.show()