#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: slion
# @Date:   2015-11-17 14:27:54
# @Last Modified by:   Septaris
# @Last Modified time: 2016-08-31 09:57:49

import datetime
import numpy
from spacepy import pycdf
from scipy import interpolate
import glob
import pandas
import pyturbu.tools.parameters as parameters
from ephemeris import Csv_ephemeris


@numpy.vectorize
def vect_to_seconds(dt):
    return dt.total_seconds()


def cluster_cdf(start_Epoch, end_Epoch, probe="C1", instruments=["FGM", "STAFF"], data_path="/home/slion/Data/cluster/"):
    data = {}
    str_datetime = start_Epoch.strftime("%Y%m%d_%H%M%S")+end_Epoch.strftime("_%Y%m%d_%H%M%S")
    for instrument in instruments:
        data[instrument] = {}
        data_type = "mag"
        if instrument == "FGM":
            filepath = "%s%s_CP_FGM_FULL/%s_CP_FGM_FULL__%s_V*.cdf" % (data_path, probe, probe, str_datetime)
            B_filekey = "B_vec_xyz_gse__%s_CP_FGM_FULL" % probe
            Epoch_filekey = "time_tags__%s_CP_FGM_FULL" % probe

        elif instrument == "STAFF":
            filepath = "%s%s_CP_STA_CWF_GSE/%s_CP_STA_CWF_GSE__%s_V*.cdf" % (data_path, probe, probe, str_datetime)
            B_filekey = "B_vec_xyz_Instrument__%s_CP_STA_CWF_GSE" % probe
            Epoch_filekey = "Time__%s_CP_STA_CWF_GSE" % probe
        elif instrument == "CIS":
            filepath = "%s%s_CP_CIS-HIA_ONBOARD_MOMENTS/%s_CP_CIS-HIA_ONBOARD_MOMENTS__%s_V*.cdf" % (data_path, probe, probe, str_datetime)
            V_filekey = "velocity_gse__%s_CP_CIS-HIA_ONBOARD_MOMENTS" % probe
            T_filekey = "temperature__%s_CP_CIS-HIA_ONBOARD_MOMENTS" % probe
            n_filekey = "density__%s_CP_CIS-HIA_ONBOARD_MOMENTS" % probe
            Epoch_filekey = "time_tags__%s_CP_CIS-HIA_ONBOARD_MOMENTS" % probe
            data_type = "proton"
        cdffile = pycdf.CDF(glob.glob(filepath)[-1])
        Epoch = cdffile[Epoch_filekey][...]
        index = numpy.logical_and(Epoch >= start_Epoch, Epoch <= end_Epoch)
        data[instrument]["Epoch"] = Epoch[index]
        data[instrument]["sample_frequency"] = 1./(numpy.mean(vect_to_seconds(numpy.diff(Epoch[index]))))
        if data_type == "mag":
            data[instrument]["Bx"] = cdffile[B_filekey][:, 0][index]
            data[instrument]["By"] = cdffile[B_filekey][:, 1][index]
            data[instrument]["Bz"] = cdffile[B_filekey][:, 2][index]
        else:
            data[instrument]["Vx"] = cdffile[V_filekey][:, 0][index]
            data[instrument]["Vy"] = cdffile[V_filekey][:, 1][index]
            data[instrument]["Vz"] = cdffile[V_filekey][:, 2][index]
            data[instrument]["temperature"] = cdffile[T_filekey][:][index]
            data[instrument]["density"] = cdffile[n_filekey][:][index]
    return data


def wind_cdf(start_Epoch, end_Epoch, data_path="/home/slion/Data/wind/"):
    MFI_data = pycdf.CDF(start_Epoch.strftime(data_path+"wi_h2_mfi_%Y%m%d_v05.cdf"))
    Bx = MFI_data["BGSE"][:, 0]
    By = MFI_data["BGSE"][:, 1]
    Bz = MFI_data["BGSE"][:, 2]
    Epoch = MFI_data["Epoch"][:, 0]
    index = numpy.logical_and(Epoch >= start_Epoch, Epoch <= end_Epoch)

    return 1./(numpy.mean(vect_to_seconds(numpy.diff(Epoch[index])))), Bx[index], By[index], Bz[index]


def wind_pandas_cdf(start_Epoch, end_Epoch, data_path="./data/wind/"):

    # data from http://cdaweb.gsfc.nasa.gov/cgi-bin/eval2.cgi

    current_Epoch = start_Epoch

    all_data = []
    while current_Epoch <= end_Epoch:

        mag_filename = current_Epoch.strftime(data_path+"wi_h2_mfi_%Y%m%d_v05.cdf")
        proton_filename = current_Epoch.strftime(data_path+"wi_h1_swe_%Y%m%d_v01.cdf")

        mag_data = pycdf.CDF(mag_filename)
        proton_data = pycdf.CDF(proton_filename)
        # print((mag_data["Epoch"][2001]-mag_data["Epoch"][2000])[0].total_seconds())
        # mag_data.close()
        # exit()
        # print(proton_data["Epoch"][...])
        mag_datetime_index = pandas.DatetimeIndex(mag_data["Epoch"][:, 0])
        proton_datetime_index = pandas.DatetimeIndex(proton_data["Epoch"][...])

        mag_pandas_data = pandas.DataFrame({"B1": mag_data["BGSE"][:, 0],
                                            "B2": mag_data["BGSE"][:, 1],
                                            "B3": mag_data["BGSE"][:, 2],
                                            },
                                           index=mag_datetime_index)

        proton_pandas_data = pandas.DataFrame({"V1": proton_data["Proton_VX_nonlin"][...],
                                               "V2": proton_data["Proton_VY_nonlin"][...],
                                               "V3": proton_data["Proton_VZ_nonlin"][...],
                                               "density_proton": proton_data["Proton_Np_nonlin"][...],
                                               "temperature_proton": parameters.ion_temperature(proton_data["Proton_W_nonlin"][...]),
                                               },
                                              index=proton_datetime_index)

        proton_pandas_data[proton_pandas_data < -1e30] = numpy.nan

        all_data.append(pandas.merge(proton_pandas_data, mag_pandas_data, how='outer', left_index=True, right_index=True))

        current_Epoch += datetime.timedelta(days=1)

        mag_data.close()
        proton_data.close()

    return pandas.concat(all_data, join='outer',)


def stereo_pandas_cdf(start_Epoch, end_Epoch, probe="STA", data_path="/home/slion/Data/stereo/mag_pla/"):
    # data from http://cdaweb.gsfc.nasa.gov/cgi-bin/eval2.cgi

    current_Epoch = start_Epoch

    all_data = []
    while current_Epoch <= end_Epoch:

        mag_filename = current_Epoch.strftime(data_path+probe.lower()+"_l1_mag_rtn_%Y%m%d_v06.cdf")
        proton_filename = current_Epoch.strftime(data_path+probe.lower()+"_l2_pla_1dmax_1min_%Y%m%d_v09.cdf")

        mag_data = pycdf.CDF(mag_filename)
        proton_data = pycdf.CDF(proton_filename)
        # print((mag_data["Epoch"][2001]-mag_data["Epoch"][2000])[0].total_seconds())
        # mag_data.close()
        # exit()
        # print(proton_data["Epoch"][...])
        mag_datetime_index = pandas.DatetimeIndex(mag_data["Epoch"][...])
        proton_datetime_index = pandas.DatetimeIndex(proton_data["epoch"][...])
        stereo_ephemeris_kernel = Csv_ephemeris(probe.upper()+"-SSB.csv", data_path=data_path)
        sun_ephemeris_kernel = Csv_ephemeris("SUN-SSB.csv", data_path=data_path)
        stereo_ephemeris_values = stereo_ephemeris_kernel.compute(proton_data["epoch"][...])
        sun_ephemeris_values = sun_ephemeris_kernel.compute(proton_data["epoch"][...])
        heliocentric_distance = numpy.sqrt((stereo_ephemeris_values["X"]-sun_ephemeris_values["X"])**2+
                                           (stereo_ephemeris_values["Y"]-sun_ephemeris_values["Y"])**2+
                                           (stereo_ephemeris_values["Z"]-sun_ephemeris_values["Z"])**2)
        mag_pandas_data = pandas.DataFrame({"B1": mag_data["BFIELD"][:, 0],
                                            "B2": mag_data["BFIELD"][:, 1],
                                            "B3": mag_data["BFIELD"][:, 2],
                                            },
                                           index=mag_datetime_index)

        proton_pandas_data = pandas.DataFrame({"V1": proton_data["proton_Vr_RTN"][...],
                                               "V2": proton_data["proton_Vt_RTN"][...],
                                               "V3": proton_data["proton_Vn_RTN"][...],
                                               "density_proton": proton_data["proton_number_density"][...],
                                               "temperature_proton": proton_data["proton_temperature"][...],
                                               "heliocentric_distance": heliocentric_distance/149597871.
                                               },
                                              index=proton_datetime_index)

        proton_pandas_data[proton_pandas_data < -1e30] = numpy.nan

        all_data.append(pandas.merge(proton_pandas_data, mag_pandas_data, how='outer', left_index=True, right_index=True))

        current_Epoch += datetime.timedelta(days=1)

        mag_data.close()
        proton_data.close()

    if len(all_data) == 0:
        print(all_data)
        return None, None
    return pandas.concat(all_data, join='outer',), probe.lower()


def stereo_cdf(start_Epoch, end_Epoch, probe="STA", use_pla_data=True, use_mag_data=True, data_path="/home/slion/Data/stereo/mag_pla/"):

    # data from http://cdaweb.gsfc.nasa.gov/cgi-bin/eval2.cgi

    # Set the current day

    current_Epoch = start_Epoch

    # Prepare dictionary to store data to interpolate
    if (use_pla_data and use_mag_data):
        all_data = {"Br": [],
                    "Bt": [],
                    "Bn": [],
                    "mag_index": [],
                    "Vr": [],
                    "Vt": [],
                    "Vn": [],
                    "density": [],
                    "temperature": [],
                    "proton_index": [],
                    }
    elif use_pla_data:
        all_data = {"Vr": [],
                    "Vt": [],
                    "Vn": [],
                    "density": [],
                    "temperature": [],
                    "proton_index": [],
                    }
    elif use_mag_data:
        all_data = {"Br": [],
                    "Bt": [],
                    "Bn": [],
                    "mag_index": [],
                    }

    # Loop over all the days

    while current_Epoch < end_Epoch:
        if use_mag_data:
            mag_filename = current_Epoch.strftime(data_path+probe.lower()+"_l1_mag_rtn_%Y%m%d_v06.cdf")
            mag_data = pycdf.CDF(mag_filename)
            all_data["Br"].append(mag_data["BFIELD"][:, 0])
            all_data["Bt"].append(mag_data["BFIELD"][:, 1])
            all_data["Bn"].append(mag_data["BFIELD"][:, 2])
            all_data["mag_index"].append(mag_data["Epoch"][...])
            mag_data.close()
        if use_pla_data:
            proton_filename = current_Epoch.strftime(data_path+probe.lower()+"_l2_pla_1dmax_1min_%Y%m%d_v09.cdf")
            proton_data = pycdf.CDF(proton_filename)
            all_data["Vr"].append(proton_data["proton_Vr_RTN"][...])
            all_data["Vt"].append(proton_data["proton_Vt_RTN"][...])
            all_data["Vn"].append(proton_data["proton_Vn_RTN"][...])
            all_data["density"].append(proton_data["proton_number_density"][...])
            all_data["temperature"].append(proton_data["proton_temperature"][...])
            all_data["proton_index"].append(proton_data["epoch"][...])
            proton_data.close()

        current_Epoch += datetime.timedelta(days=1)

    # Change list of array to array and check for bad data
    for key in all_data:
        all_data[key] = numpy.concatenate(all_data[key])
        if ((key != "mag_index") and (key != "proton_index")):
            all_data[key][all_data[key] < -1e30] = numpy.nan

    # Change datetime (both mag and proton) object to seconds from mag start index
    if use_mag_data:
        mag_seconds_from_start = vect_to_seconds(all_data["mag_index"] - start_Epoch)
    if use_pla_data:
        proton_seconds_from_start = vect_to_seconds(all_data["proton_index"] - start_Epoch)

    # New dictionary to store interpolated data

    interp_data = {}

    # Store index to recover the real datapoints if needed
    if use_mag_data:
        interp_data["mag_index"] = all_data["mag_index"]
    if use_pla_data:
        interp_data["proton_index"] = all_data["proton_index"]

    # Store interpolated functions (NOT data)

    if use_mag_data:
        interp_data["Br"] = interpolate.interp1d(mag_seconds_from_start, all_data["Br"], bounds_error=False, )
        interp_data["Bt"] = interpolate.interp1d(mag_seconds_from_start, all_data["Bt"], bounds_error=False, )
        interp_data["Bn"] = interpolate.interp1d(mag_seconds_from_start, all_data["Bn"], bounds_error=False, )
    if use_pla_data:
        interp_data["Vr"] = interpolate.interp1d(proton_seconds_from_start, all_data["Vr"], bounds_error=False, )
        interp_data["Vt"] = interpolate.interp1d(proton_seconds_from_start, all_data["Vt"], bounds_error=False, )
        interp_data["Vn"] = interpolate.interp1d(proton_seconds_from_start, all_data["Vn"], bounds_error=False, )
        interp_data["density"] = interpolate.interp1d(proton_seconds_from_start, all_data["density"], bounds_error=False, )
        interp_data["temperature"] = interpolate.interp1d(proton_seconds_from_start, all_data["temperature"], bounds_error=False, )

    # Clean memory and return functions
    del all_data
    return interp_data


def ulysses_cdf(start_Epoch, end_Epoch, data_path="./data/ulysses/"):

    # data from http://cdaweb.gsfc.nasa.gov/cgi-bin/eval2.cgi

    current_Epoch = start_Epoch

    all_data = []
    while current_Epoch <= end_Epoch:

        mag_filename = current_Epoch.strftime(data_path+"uy_1sec_vhm_%Y%m%d_v01.cdf")
        proton_filename = current_Epoch.strftime(data_path+"uy_m0_bai_%Y%m%d_v01.cdf")

        mag_data = pycdf.CDF(mag_filename)
        proton_data = pycdf.CDF(proton_filename)

        mag_datetime_index = pandas.DatetimeIndex(mag_data["Epoch"][...])
        proton_datetime_index = pandas.DatetimeIndex(proton_data["Epoch"][...])

        ulysses_ephemeris_kernel = Csv_ephemeris("ULYSSES-SSB.csv", data_path=data_path)
        sun_ephemeris_kernel = Csv_ephemeris("SUN-SSB.csv", data_path=data_path)
        ulysses_ephemeris_values = ulysses_ephemeris_kernel.compute(proton_data["Epoch"][...])
        sun_ephemeris_values = sun_ephemeris_kernel.compute(proton_data["Epoch"][...])
        heliocentric_distance = numpy.sqrt((ulysses_ephemeris_values["X"]-sun_ephemeris_values["X"])**2+
                                           (ulysses_ephemeris_values["Y"]-sun_ephemeris_values["Y"])**2+
                                           (ulysses_ephemeris_values["Z"]-sun_ephemeris_values["Z"])**2)

        mag_pandas_data = pandas.DataFrame({"B1": mag_data["B_RTN"][:, 0],
                                            "B2": mag_data["B_RTN"][:, 1],
                                            "B3": mag_data["B_RTN"][:, 2],
                                            },
                                           index=mag_datetime_index)

        proton_pandas_data = pandas.DataFrame({"V1": proton_data["Velocity"][:, 0],
                                               "V2": proton_data["Velocity"][:, 1],
                                               "V3": proton_data["Velocity"][:, 2],
                                               "density_proton": proton_data["Density"][:, 0],
                                               "Density_alpha": proton_data["Density"][:, 1],
                                               "temperature_proton": proton_data["Temperature"][:, 0],
                                               "Temperature_small": proton_data["Temperature"][:, 1],
                                               "heliocentric_distance": heliocentric_distance/149597871.
                                               },
                                              index=proton_datetime_index)

        all_data.append(pandas.merge(proton_pandas_data, mag_pandas_data, how='outer', left_index=True, right_index=True))

        current_Epoch += datetime.timedelta(days=1)

        mag_data.close()
        proton_data.close()
    if len(all_data) == 0:
        print(all_data)
        return None, None

    return pandas.concat(all_data, join='outer',), "ulysses"


def helios_cdf(start_Epoch, end_Epoch, probe="2", data_path="../data/helios/helios2/"):

    # data from http://cdaweb.gsfc.nasa.gov/cgi-bin/eval2.cgi

    current_Epoch = start_Epoch

    all_data = []
    while current_Epoch <= end_Epoch:
        mag_filename = current_Epoch.strftime(data_path+"hel2_6sec_nessmag_%Y%m%d_v01.cdf")
        proton_filename = current_Epoch.strftime(data_path+"helios2_40sec_mag-plasma_%Y%m%d_v01.cdf")

        try:
            mag_data = pycdf.CDF(mag_filename)
        except Exception, e:
            print("problem reading "+mag_filename)
            print("switch to next file")
            current_Epoch += datetime.timedelta(days=1)
            continue

        try:
            proton_data = pycdf.CDF(proton_filename)
        except Exception, e:
            mag_data.close()
            print("problem reading "+proton_filename)
            print("switch to next file")
            current_Epoch += datetime.timedelta(days=1)
            continue

        mag_datetime_index = pandas.DatetimeIndex(mag_data["Epoch"][...])
        proton_datetime_index = pandas.DatetimeIndex(proton_data["Epoch"][...])

        mag_pandas_data = pandas.DataFrame({"B1": mag_data["BXSSE"][...],
                                            "B2": mag_data["BYSSE"][...],
                                            "B3": mag_data["BZSSE"][...],
                                            },
                                           index=mag_datetime_index)

        proton_pandas_data = pandas.DataFrame({"V1": proton_data["Vp_R"][...],
                                               "V2": proton_data["Vp_T"][...],
                                               "V3": proton_data["Vp_N"][...],
                                               "density_proton": proton_data["Np"][...],
                                               "temperature_proton": proton_data["Tp"][...],
                                               "heliocentric_distance": proton_data["R_Helio"][...],
                                               },
                                              index=proton_datetime_index)

        proton_pandas_data[numpy.abs(proton_pandas_data) > 1e30] = numpy.nan

        all_data.append(pandas.merge(proton_pandas_data, mag_pandas_data, how='outer', left_index=True, right_index=True).interpolate(
            "time").drop_duplicates().reindex(index=mag_datetime_index).drop_duplicates())

        current_Epoch += datetime.timedelta(days=1)

        mag_data.close()
        proton_data.close()
    if len(all_data) == 0:
        print(all_data)
        return None, None
    return pandas.concat(all_data, join='outer',), "helios"+probe

if __name__ == '__main__':
    data = cluster_cdf(datetime.datetime(2007, 1, 30, 0, 10, 0), datetime.datetime(2007, 1, 30, 1, 10, 0), "C4")
    print(data)
