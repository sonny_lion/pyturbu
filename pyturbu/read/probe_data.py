#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: slion
# @Date:   2015-01-29 14:14:32
# @Last Modified by:   slion
# @Last Modified time: 2015-10-16 15:03:05

import numpy
import pyturbu.tools.utility as utility
import wind_cdf
import stereo_cdf
import cluster_sav
import pyturbu.tools.geometry as geometry

# BUG Changement de reference pour la vitesse


class Probe_data:

    """Magnetic data with time index and reference frame"""
    vectx = [1, 0, 0]
    vecty = [0, 1, 0]
    vectz = [0, 0, 1]

    def __init__(self, probe, start_time, end_time, download=False):
        # http://stackoverflow.com/questions/1305532/convert-python-dict-to-object
        if probe == "WIND":
            entries = wind_cdf.read(start_time, end_time, download=download)
            self.__dict__.update(entries)
        elif probe == "STA":
            entries = stereo_cdf.read(start_time, end_time, "STA", download=download)
            self.__dict__.update(entries)
        elif probe == "STB":
            entries = stereo_cdf.read(start_time, end_time, "STB", download=download)
            self.__dict__.update(entries)
        elif probe == "CLUSTER":
            entries = cluster_sav.read()
            self.__dict__.update(entries)
        else:
            print("Error, no data for: "+probe)

        self.inv_P_matrix = numpy.linalg.inv(numpy.array([self.vectx, self.vecty, self.vectz]).transpose())

    def __getitem__(self, item):
        return self.__dict__[item]

    def get_lf_magnetic_data(self, start_time=None, end_time=None):
        if start_time == None:
            start_time = self.MAG_LF["Epoch"][0]
        if end_time == None:
            end_time = self.MAG_LF["Epoch"][-1]
        time_interval = numpy.logical_and(self.MAG_LF["Epoch"] >= start_time, self.MAG_LF["Epoch"] <= end_time)
        data = [self.MAG_LF["Bx"][time_interval], self.MAG_LF["By"][time_interval], self.MAG_LF["Bz"][time_interval]]
        new_data = numpy.dot(self.inv_P_matrix, data)
        return new_data[0], new_data[1], new_data[2]

    def change_ref_frame(self, v1, v2, v3):
        P = numpy.array([v1, v2, v3]).transpose()
        self.inv_P_matrix = numpy.linalg.inv(P)
        self.vectx = v1
        self.vecty = v2
        self.vectz = v3

    def B_LF_modulus(self):
        return numpy.sqrt(self.MAG_LF["Bx"]*self.MAG_LF["Bx"] +
                          self.MAG_LF["By"]*self.MAG_LF["By"] +
                          self.MAG_LF["Bz"]*self.MAG_LF["Bz"])

    def B_LF_mean(self, start_time=None, end_time=None, use_ref_frame=True):
        if start_time is None:
            start_time = self.MAG_LF["Epoch"][0]
        if end_time is None:
            end_time = self.MAG_LF["Epoch"][-1]
        time_interval = numpy.logical_and(self.MAG_LF["Epoch"] >= start_time, self.MAG_LF["Epoch"] <= end_time)

        B_data = numpy.dot(self.inv_P_matrix, [self.MAG_LF["Bx"], self.MAG_LF["By"], self.MAG_LF["Bz"]])
        if use_ref_frame:
            return numpy.array([numpy.mean(B_data[0][time_interval]),
                                numpy.mean(B_data[1][time_interval]),
                                numpy.mean(B_data[2][time_interval])])
        else:
            return numpy.array([numpy.mean(self.MAG_LF["Bx"][time_interval]),
                                numpy.mean(self.MAG_LF["By"][time_interval]),
                                numpy.mean(self.MAG_LF["Bz"][time_interval])])

    def V_modulus(self):
        return numpy.sqrt(self.Proton["Vx"]*self.Proton["Vx"]+self.Proton["Vy"]*self.Proton["Vy"]+self.Proton["Vz"]*self.Proton["Vz"])

    def V_mean(self, start_time=None, end_time=None, use_ref_frame=True):
        if start_time == None:
            start_time = self.MAG_LF["Epoch"][0]
        if end_time == None:
            end_time = self.MAG_LF["Epoch"][-1]
        time_interval = numpy.logical_and(self.Proton["Epoch"] >= start_time, self.Proton["Epoch"] <= end_time)
        if use_ref_frame:
            V_data = numpy.dot(self.inv_P_matrix,
                               [self.Proton["Vx"],
                                self.Proton["Vy"],
                                self.Proton["Vz"]])

            return numpy.array([numpy.mean(V_data[0][time_interval]),
                                numpy.mean(V_data[1][time_interval]),
                                numpy.mean(V_data[2][time_interval])])
        else:
            return numpy.array([numpy.mean(self.Proton["Vx"][time_interval]),
                                numpy.mean(self.Proton["Vy"][time_interval]),
                                numpy.mean(self.Proton["Vz"][time_interval])])

    def proton_dt(self):
        return (self.Proton["Epoch"][1]-self.Proton["Epoch"][0]).total_seconds()

    def lf_mag_dt(self):
        return (self.MAG_LF["Epoch"][1]-self.MAG_LF["Epoch"][0]).total_seconds()

    def smooth(self, smoothing_freq):
        B_data = numpy.dot(self.inv_P_matrix, [self.MAG_LF["Bx"], self.MAG_LF["By"], self.MAG_LF["Bz"]])
        window_size = int((1.0/smoothing_freq)/self.lf_mag_dt())+1

        Bx_smooth = utility.window_smooth(B_data[0], window_size)
        By_smooth = utility.window_smooth(B_data[1], window_size)
        Bz_smooth = utility.window_smooth(B_data[2], window_size)
        return Bx_smooth, By_smooth, Bz_smooth

    def BV_frame(self, start_time=None, end_time=None):
        self.change_ref_frame([1, 0, 0], [0, 1, 0], [0, 0, 1])  # Remise a zero
        ux = self.B_LF_mean(start_time, end_time)
        self.vectx = ux/geometry.norm(ux)

        V = self.V_mean(start_time, end_time)

        uy = geometry.cross_product(ux, V)
        self.vecty = uy/geometry.norm(uy)

        uz = geometry.cross_product(ux, uy)
        self.vectz = uz/geometry.norm(uz)
        self.inv_P_matrix = numpy.linalg.inv(numpy.array([self.vectx, self.vecty, self.vectz]).transpose())

    def BR_frame(self, start_time=None, end_time=None):
        self.change_ref_frame([1, 0, 0], [0, 1, 0], [0, 0, 1])  # Remise a zero
        ux = self.B_LF_mean(start_time, end_time)
        self.vectx = ux/geometry.norm(ux)

        R = [1.0, 0.0, 0.0]

        uy = geometry.cross_product(ux, R)
        self.vecty = uy/geometry.norm(uy)

        uz = geometry.cross_product(ux, uy)
        self.vectz = uz/geometry.norm(uz)
        self.inv_P_matrix = numpy.linalg.inv(numpy.array([self.vectx, self.vecty, self.vectz]).transpose())

    def min_frame(self, start_time=None, end_time=None, smoothing_freq=None):
        self.change_ref_frame([1, 0, 0], [0, 1, 0], [0, 0, 1])  # Remise a zero
        if start_time == None:
            start_time = self.MAG_LF["Epoch"][0]
        if end_time == None:
            end_time = self.MAG_LF["Epoch"][-1]
        time_interval = numpy.logical_and(self.MAG_LF["Epoch"] >= start_time, self.MAG_LF["Epoch"] <= end_time)
        if smoothing_freq == None:
            B_data = [self.MAG_LF["Bx"][time_interval], self.MAG_LF["By"][time_interval], self.MAG_LF["Bz"][time_interval]]
        else:
            Bx_smooth, By_smooth, Bz_smooth = self.smooth(smoothing_freq)
            B_data = [self.MAG_LF["Bx"][time_interval]-Bx_smooth[time_interval],
                      self.MAG_LF["By"][time_interval]-By_smooth[time_interval],
                      self.MAG_LF["Bz"][time_interval]-Bz_smooth[time_interval]]
        cov_B_data = numpy.cov(B_data)

        eigenvalues, eigenvectors = numpy.linalg.eig(cov_B_data)
        print("eigenvalues", eigenvalues)
        eigenvalues = eigenvalues/numpy.amax(eigenvalues)
        print("relativ eigenvalues", eigenvalues)
        maxindex = eigenvalues.argmax()
        minindex = eigenvalues.argmin()
        e_min = eigenvectors[:, minindex]
        e_max = eigenvectors[:, maxindex]
        B0 = self.B_LF_mean()
        align_with_B0 = numpy.vdot(e_min, B0)/geometry.norm(B0)
        if align_with_B0 < 0:
            e_min = (-1.0)*e_min
        e_med = geometry.cross_product(e_min, e_max)

        for idx, x in enumerate(eigenvalues):
            if (minindex == idx):
                value_min = x
            elif (maxindex == idx):
                value_max = x
            else:
                value_med = x

        self.vectx = e_max
        self.vecty = e_med
        self.vectz = e_min
        self.inv_P_matrix = numpy.linalg.inv(numpy.array([self.vectx, self.vecty, self.vectz]).transpose())

    def draw_ref_frame(self, ax_ref_frame):

        a = geometry.Arrow3D([0, 0, 0], [1, 0, 0], mutation_scale=20, lw=1, arrowstyle="-|>", color="black")
        ax_ref_frame.add_artist(a)
        a = geometry.Arrow3D([0, 0, 0], [0, 1, 0], mutation_scale=20, lw=1, arrowstyle="-|>", color="black")
        ax_ref_frame.add_artist(a)
        a = geometry.Arrow3D([0, 0, 0], [0, 0, 1], mutation_scale=20, lw=1, arrowstyle="-|>", color="black")
        ax_ref_frame.add_artist(a)

        a = geometry.Arrow3D([0, 0, 0], self.vectx, mutation_scale=20, lw=2, arrowstyle="-|>", color="blue")
        ax_ref_frame.add_artist(a)
        a = geometry.Arrow3D([0, 0, 0], self.vecty, mutation_scale=20, lw=2, arrowstyle="-|>", color="red")
        ax_ref_frame.add_artist(a)
        a = geometry.Arrow3D([0, 0, 0], self.vectz, mutation_scale=20, lw=2, arrowstyle="-|>", color="green")
        ax_ref_frame.add_artist(a)
