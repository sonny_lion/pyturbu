#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: Sonny
# @Date:   2015-01-26 21:13:41
# @Last Modified by:   slion
# @Last Modified time: 2015-07-29 17:33:03

# -----------------------------------------------------------------------------
# Filename:    stereo_cdf.py
# -----------------------------------------------------------------------------
#
# Sonny LION - sonny.lion@obspm.fr
# Doctorant - PhD Student
# Laboratoire d'Etudes Spatiales et d'Instrumentation en Astrophysique (LESIA)
# -----------------------------------------------------------------------------

import datetime
import numpy
import pandas
from spacepy import pycdf
import urllib
import pyturbu.read
import os
import sys
import pyturbu.tools.utility as utility


def read(input_start_Epoch, input_end_Epoch, probe="STA", data_path="/home/slion/Data/stereo/", download=False):

    # Exemple: read("2008-01-30/13:00:00.0","2008-01-30/14:00:00.0")
    #
    # A changer: hard coded sample rate
    # get data at http://cdaweb.sci.gsfc.nasa.gov/istp_public/
    #
    # Data structure used:
    #
    # Data/
    #     MAG_LF/
    #         Bx
    #         By
    #         Bz
    #         Epoch
    #         Info/
    #             Instrument
    #             Version
    #             Ref_frame
    #             Unit
    #     Proton/
    #         Vx
    #         Vy
    #         Vz
    #         N
    #         T
    #         T_perp
    #         T_para
    #         Epoch
    #         Info/
    #             Instrument
    #             Version
    #             Ref_frame
    #             V_unit
    #             N_unit
    #             T_unit
    #     Probe
    #

    Info_MAG = {'Instrument': "IMPACT/MAG", 'Version': '06', 'Ref_frame': 'RTN', "Unit": "nT"}
    Info_PLA = {'Instrument': "PLASTIC", 'Version': '09', 'Ref_frame': 'RTN', "V_unit": "km/s", "N_unit": "1/cc", "T_unit": "K"}

    probe_mapping = {"STA": "ahead", "STB": "behind"}

    data =\
        dict(
            MAG_LF=dict(
                Info=Info_MAG),
            Proton=dict(
                Info=Info_PLA),
            Probe=probe,
        )

    if type(input_start_Epoch) is datetime.datetime:
        start_Epoch = input_start_Epoch
    else:
        start_Epoch = datetime.datetime.strptime(input_start_Epoch, "%Y-%m-%d/%H:%M:%S.%f")
    if type(input_start_Epoch) is datetime.datetime:
        end_Epoch = input_end_Epoch
    else:
        end_Epoch = datetime.datetime.strptime(input_end_Epoch, "%Y-%m-%d/%H:%M:%S.%f")

    download_url = "ftp://cdaweb.gsfc.nasa.gov/pub/data/stereo/"+probe_mapping[probe]
    download_path = "%s/download/stereo/" % os.path.dirname(pyturbu.read.__file__)

    # IMPACT/MAG Data

    MAG_filename = start_Epoch.strftime(probe.lower()+"_l1_mag_rtn_%Y%m%d_v06.cdf")

    try:
        MAG_data = pycdf.CDF(data_path+MAG_filename)
    except Exception, e:
        print(e)
        print(data_path+MAG_filename)
        try:
            MAG_data = pycdf.CDF(download_path+MAG_filename)
        except Exception, e:
            print(e)
            print(download_path+MAG_filename)
            if download:
                try:
                    print("\ntry to download file from cdaweb")
                    MAG_url = start_Epoch.strftime("/l1/impact/rtn/mag/%Y/")
                    sys.stdout.write(MAG_filename + "...")
                    urllib.urlretrieve(download_url+MAG_url+MAG_filename, download_path+MAG_filename, reporthook=utility.dlProgress)
                    print("\nfile save at: %s%s" % (download_path, MAG_filename))
                    try:
                        MAG_data = pycdf.CDF(download_path+MAG_filename)
                    except Exception, e:
                        print(e)
                        print("fail to load downloaded file\n")
                        return None
                except Exception, e:
                    print(e)
                    print("fail to download file\n")
                    return None
            else:
                print("file not found, try to active download in order to retrieve the selected file")
                return None

    datetime_index = pandas.DatetimeIndex(MAG_data["Epoch"][...])

    MAG_pandas_data = pandas.DataFrame({"Bx": MAG_data["BFIELD"][:, 0], "By": MAG_data["BFIELD"][:, 1], "Bz": MAG_data["BFIELD"][:, 2], }, index=datetime_index)

    MAG_data.close()

    # Plastic Data

    PLA_filename = start_Epoch.strftime(probe.lower()+"_l2_pla_1dmax_1min_%Y%m%d_v09.cdf")

    try:
        PLA_data = pycdf.CDF(data_path+PLA_filename)
    except Exception, e:
        print(e)
        print(data_path+PLA_filename)
        try:
            PLA_data = pycdf.CDF(download_path+PLA_filename)
        except Exception, e:
            print(e)
            print(download_path+PLA_filename)
            if download:
                try:
                    print("\ntry to download file from cdaweb")
                    PLA_url = start_Epoch.strftime("/l2/plastic/1dmax/1min/%Y/")
                    sys.stdout.write(PLA_filename + "...")
                    urllib.urlretrieve(download_url+PLA_url+PLA_filename, download_path+PLA_filename, reporthook=utility.dlProgress)
                    print("\nfile save at: %s%s" % (download_path, PLA_filename))
                    try:
                        PLA_data = pycdf.CDF(download_path+PLA_filename)
                    except Exception, e:
                        print(e)
                        print("fail to load downloaded file\n")
                        return None
                except Exception, e:
                    print(e)
                    print("fail to download file\n")
                    return None
            else:
                print("file not found, try to active download in order to retrieve the selected file")
                return None

    PLA_datetime_index = pandas.DatetimeIndex(PLA_data["epoch"][...])

    PLA_pandas_data = pandas.DataFrame({"V_x": PLA_data["proton_Vr_RTN"][...],
                                        "V_y": PLA_data["proton_Vt_RTN"][...],
                                        "V_z": PLA_data["proton_Vn_RTN"][...],
                                        "Temperature": PLA_data["proton_temperature"][...],
                                        "Density": PLA_data["proton_number_density"][...], },
                                       index=PLA_datetime_index)

    PLA_data.close()

    all_pandas_data = pandas.merge(MAG_pandas_data, PLA_pandas_data, how='outer', left_index=True, right_index=True)
    all_pandas_data_index = all_pandas_data.index.to_pydatetime()
    good_index = numpy.logical_and(all_pandas_data_index >= datetime_index[0], all_pandas_data_index <= datetime_index[-1])
    resampled = all_pandas_data.interpolate("time")[good_index].resample('125L').interpolate()

    good_index = numpy.logical_and(resampled.index.to_pydatetime() >= start_Epoch,
                                   resampled.index.to_pydatetime() <= end_Epoch)
    data["MAG_LF"]["Epoch"] = resampled.index.to_pydatetime()[good_index]
    data["MAG_LF"]["Bx"] = resampled['Bx'].values[good_index]
    data["MAG_LF"]["By"] = resampled['By'].values[good_index]
    data["MAG_LF"]["Bz"] = resampled['Bz'].values[good_index]

    data["Proton"]["Epoch"] = resampled.index.to_pydatetime()[good_index]
    data["Proton"]["Vx"] = resampled["V_x"].values[good_index]
    data["Proton"]["Vy"] = resampled["V_y"].values[good_index]
    data["Proton"]["Vz"] = resampled["V_z"].values[good_index]
    data["Proton"]["T"] = resampled["Temperature"][good_index]
    data["Proton"]["T_perp"] = resampled["Temperature"][good_index]
    data["Proton"]["T_para"] = resampled["Temperature"][good_index]
    data["Proton"]["N"] = resampled["Density"].values[good_index]

    return data


if __name__ == '__main__':

    data = read("2008-01-30/13:00:00.0", "2008-01-30/14:00:00.0")

    print(data)
    exit()
