Installing Pyturbu
+++++++++++++++++++++++++++++

.. Contents::

INTRODUCTION
============

It is *strongly* recommended that you use either a complete scientific Python
distribution or binary packages on your platform if they are available, in
particular on Windows and Mac OS X.

Recommended distribution are:

  - Anaconda (https://store.continuum.io/cshop/anaconda)
  - Miniconda: Minimal installation that only includes conda and its dependencies (http://conda.pydata.org/miniconda.html)


PREREQUISITES
=============

Pyturbu requires the following software installed for your platform: see `requirements.txt`

GETTING PYTURBU
=============

For the latest information, see the git repository:

  https://bitbucket.org/sonny_lion/pyturbu


Development version from Git
----------------------------
Use the command::

  git clone https://sonny_lion@bitbucket.org/sonny_lion/pyturbu.git

Before installing from git, remove the old installation
(e.g. `pip uninstall pyturbu`).  Then type::

  cd pyturbu
  pip install .

You can install local Pyturbu in editable mode::

  cd pyturbu
  pip install -e .

Or install directly from the remote repository::

  pip install git+https://sonny_lion@bitbucket.org/sonny_lion/pyturbu.git