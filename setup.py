#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: slion
# @Date:   2015-03-10 09:54:10
# @Last Modified by:   Septaris
# @Last Modified time: 2016-08-30 11:45:41

from setuptools import setup, find_packages

setup(name='pyturbu',
      version='0.20',
      description='Python Turbulence Utilities',
      install_requires=['numpy', 'scipy', 'matplotlib', 'seaborn', 'spacepy'],
      url='https://bitbucket.org/sonny_lion/pyturbu',
      author='Sonny Lion',
      long_description=open('README.md').read(),
      author_email='sonny.lion@obspm.fr',
      packages=find_packages())
